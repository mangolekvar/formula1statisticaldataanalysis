﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Controllers;
    using Formula1StatisticalAnalysis.Data;
    using Formula1StatisticalAnalysis.Models;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Unit test class.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        private Mock<ICircuitRepository> circuitRepo;
        private Mock<IDriverRatingRepository> driverRatingRepo;
        private Mock<IDriverRepository> driverRepo;
        private Mock<IFailureRepository> failureRepo;
        private Mock<ILapTimeRepository> laptimeRepo;
        private Mock<IRaceRepository> raceRepo;
        private Mock<IResultRepository> resultRepo;
        private Mock<IStatusRepository> statusRepo;

        private ControllerLogic logic;

        private List<Circuit> circuits;
        private List<DriverRatings> driverRatings;
        private List<Driver> drivers;
        private List<Failure> failures;
        private List<LapTime> laptimes;
        private List<Race> races;
        private List<Result> results;
        private List<Status> statuses;

        /// <summary>
        /// Setup method for the tests.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.circuits = new List<Circuit>();
            this.driverRatings = new List<DriverRatings>();
            this.drivers = new List<Driver>();
            this.failures = new List<Failure>();
            this.laptimes = new List<LapTime>();
            this.races = new List<Race>();
            this.results = new List<Result>();
            this.statuses = new List<Status>();

            this.circuits.Add(new Circuit(1, "Szolnok City Circuit", "Szolnok", "Hungary"));

            this.drivers.Add(new Driver(1, "Mango Lekvar"));
            this.drivers.Add(new Driver(2, "Dominik Shadow"));

            this.races.Add(new Race(1, 2021, 1, 1, "Szolnok Grand Prix"));
            this.races.Add(new Race(2, 2022, 1, 1, "Szolnok Grand Prix"));

            this.statuses.Add(new Status(1, "Finished"));
            this.statuses.Add(new Status(2, "Retired"));

            this.results.Add(new Result(1, 1, 1, 1, 1, 25, 120000, 1));
            this.results.Add(new Result(2, 1, 2, 1, 2, 18, 121000, 1));
            this.results.Add(new Result(3, 2, 1, 1, 1, 25, 178000, 1));
            this.results.Add(new Result(4, 2, 2, 1, 2, 18, 180000, 1));

            this.laptimes.Add(new LapTime(1, 1, 1, 61000));
            this.laptimes.Add(new LapTime(1, 1, 2, 59000));
            this.laptimes.Add(new LapTime(1, 2, 1, 61500));
            this.laptimes.Add(new LapTime(1, 2, 2, 59500));
            this.laptimes.Add(new LapTime(2, 1, 1, 60000));
            this.laptimes.Add(new LapTime(2, 1, 2, 58000));
            this.laptimes.Add(new LapTime(2, 2, 1, 62500));
            this.laptimes.Add(new LapTime(2, 2, 2, 57500));
            this.laptimes.Add(new LapTime(2, 1, 3, 60000));
            this.laptimes.Add(new LapTime(2, 2, 3, 60000));

            this.circuitRepo = new Mock<ICircuitRepository>();
            this.driverRatingRepo = new Mock<IDriverRatingRepository>();
            this.driverRepo = new Mock<IDriverRepository>();
            this.failureRepo = new Mock<IFailureRepository>();
            this.laptimeRepo = new Mock<ILapTimeRepository>();
            this.raceRepo = new Mock<IRaceRepository>();
            this.resultRepo = new Mock<IResultRepository>();
            this.statusRepo = new Mock<IStatusRepository>();

            this.circuitRepo.Setup(x => x.GetAll()).Returns(this.circuits.AsQueryable);
            this.driverRatingRepo.Setup(x => x.GetAll()).Returns(this.driverRatings.AsQueryable);
            this.driverRatingRepo.Setup(x => x.GetAllToList()).Returns(this.driverRatings.AsQueryable().ToList());
            this.driverRepo.Setup(x => x.GetAll()).Returns(this.drivers.AsQueryable);
            this.driverRepo.Setup(x => x.GetAllToList()).Returns(this.drivers.AsQueryable().ToList());
            this.failureRepo.Setup(x => x.GetAll()).Returns(this.failures.AsQueryable);
            this.failureRepo.Setup(x => x.GetAllToList()).Returns(this.failures.AsQueryable().ToList());
            this.laptimeRepo.Setup(x => x.GetAll()).Returns(this.laptimes.AsQueryable);
            this.laptimeRepo.Setup(x => x.GetAllToList()).Returns(this.laptimes.AsQueryable().ToList());
            this.raceRepo.Setup(x => x.GetAll()).Returns(this.races.AsQueryable);
            this.raceRepo.Setup(x => x.GetAllToList()).Returns(this.races.AsQueryable().ToList());
            this.resultRepo.Setup(x => x.GetAll()).Returns(this.results.AsQueryable);
            this.resultRepo.Setup(x => x.GetAllToList()).Returns(this.results.AsQueryable().ToList());
            this.statusRepo.Setup(x => x.GetAll()).Returns(this.statuses.AsQueryable);
            this.statusRepo.Setup(x => x.GetAllToList()).Returns(this.statuses.AsQueryable().ToList());

            this.logic = new ControllerLogic(this.statusRepo.Object, this.circuitRepo.Object, this.raceRepo.Object, this.driverRepo.Object, this.resultRepo.Object, this.laptimeRepo.Object, this.driverRatingRepo.Object, this.failureRepo.Object, 1);
        }

        /// <summary>
        /// Tests GetGrandPrixes method.
        /// </summary>
        [Test]
        public void GetGrandPrixes()
        {
            this.logic.GetGrandPrixes(2021, 1, 1, 2);

            Assert.AreEqual(2, this.logic.GrandPrixViewModel.GrandPrixes.Count());
            Assert.AreEqual("Mango Lekvar", this.logic.GrandPrixViewModel.GrandPrixes.ToArray()[0].DriverName);
            Assert.AreEqual(120000, this.logic.GrandPrixViewModel.GrandPrixes.ToArray()[0].Time);
            Assert.AreEqual(0, this.logic.GrandPrixViewModel.GrandPrixes.ToArray()[0].GapToWinner);
            Assert.AreEqual("Dominik Shadow", this.logic.GrandPrixViewModel.GrandPrixes.ToArray()[1].DriverName);
            Assert.AreEqual(119000, this.logic.GrandPrixViewModel.GrandPrixes.ToArray()[1].Time);
            Assert.AreEqual(1000, this.logic.GrandPrixViewModel.GrandPrixes.ToArray()[1].GapToWinner);
            Assert.AreEqual(2, this.logic.GrandPrixViewModel.Years.Count());
            Assert.AreEqual(2022, this.logic.GrandPrixViewModel.Years.ToArray()[0]);
            Assert.AreEqual(2021, this.logic.GrandPrixViewModel.Years.ToArray()[1]);
            Assert.AreEqual(1, this.logic.GrandPrixViewModel.Rounds.Count());
            Assert.AreEqual(1, this.logic.GrandPrixViewModel.Rounds.ToArray()[0]);
            Assert.AreEqual(2021, this.logic.GrandPrixViewModel.SelectedYear);
            Assert.AreEqual(1, this.logic.GrandPrixViewModel.SelectedRound);
            Assert.AreEqual(2, this.logic.GrandPrixViewModel.SelectedLaps.Count());
            Assert.AreEqual(true, this.logic.GrandPrixViewModel.SelectedLaps.Contains(1));
            Assert.AreEqual(true, this.logic.GrandPrixViewModel.SelectedLaps.Contains(2));
            Assert.AreEqual(2, this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.Count());
            Assert.AreEqual(2022, this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[0].Year);
            Assert.AreEqual(1, this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[0].Round);
            Assert.AreEqual("Szolnok Grand Prix", this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[0].Name);
            Assert.AreEqual(3, this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[0].NOLaps);
            Assert.AreEqual(2021, this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[1].Year);
            Assert.AreEqual(1, this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[1].Round);
            Assert.AreEqual("Szolnok Grand Prix", this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[1].Name);
            Assert.AreEqual(2, this.logic.GrandPrixViewModel.GrandPrixInfosByRaces.ToArray()[1].NOLaps);
            Assert.AreEqual(2, this.logic.GrandPrixViewModel.GetDriverNames().Count());
            Assert.AreEqual("Dominik Shadow", this.logic.GrandPrixViewModel.GetDriverNames().ToArray()[0]);
            Assert.AreEqual("Mango Lekvar", this.logic.GrandPrixViewModel.GetDriverNames().ToArray()[1]);
            Assert.AreEqual(2, this.logic.GrandPrixViewModel.GetTimes().Count());
            Assert.AreEqual(120000, this.logic.GrandPrixViewModel.GetTimes().ToArray()[0]);
            Assert.AreEqual(119000, this.logic.GrandPrixViewModel.GetTimes().ToArray()[1]);
            Assert.AreEqual(2, this.logic.GrandPrixViewModel.GetGaps().Count());
            Assert.AreEqual(0, this.logic.GrandPrixViewModel.GetGaps().ToArray()[0]);
            Assert.AreEqual(1000, this.logic.GrandPrixViewModel.GetGaps().ToArray()[1]);
        }

        /// <summary>
        /// Tests GetChampionship method.
        /// </summary>
        [Test]
        public void GetChampionship()
        {
            this.logic.GetChampionship(2022, 1, 2);

            Assert.AreEqual(2, this.logic.ChampionshipViewModel.Championships.Count());
            Assert.AreEqual("Mango Lekvar", this.logic.ChampionshipViewModel.Championships.ToArray()[0].DriverName);
            Assert.AreEqual("Dominik Shadow", this.logic.ChampionshipViewModel.Championships.ToArray()[1].DriverName);
            Assert.AreEqual(25, this.logic.ChampionshipViewModel.Championships.ToArray()[0].Points);
            Assert.AreEqual(18, this.logic.ChampionshipViewModel.Championships.ToArray()[1].Points);
            Assert.AreEqual(2, this.logic.ChampionshipViewModel.Years.Count());
            Assert.AreEqual(2022, this.logic.ChampionshipViewModel.Years.ToArray()[0]);
            Assert.AreEqual(2021, this.logic.ChampionshipViewModel.Years.ToArray()[1]);
            Assert.AreEqual(2, this.logic.ChampionshipViewModel.RoundsByYears.Count());
            Assert.AreEqual(1, this.logic.ChampionshipViewModel.RoundsByYears.ToArray()[0]);
            Assert.AreEqual(1, this.logic.ChampionshipViewModel.RoundsByYears.ToArray()[1]);
            Assert.AreEqual(2022, this.logic.ChampionshipViewModel.SelectedYear);
            Assert.AreEqual(2, this.logic.ChampionshipViewModel.SelectedRounds.Count());
            Assert.AreEqual(true, this.logic.ChampionshipViewModel.SelectedRounds.Contains(1));
            Assert.AreEqual(true, this.logic.ChampionshipViewModel.SelectedRounds.Contains(2));
            Assert.AreEqual(2, this.logic.ChampionshipViewModel.GetDriverNames().Count());
            Assert.AreEqual(true, this.logic.ChampionshipViewModel.GetDriverNames().Contains("Mango Lekvar"));
            Assert.AreEqual(true, this.logic.ChampionshipViewModel.GetDriverNames().Contains("Dominik Shadow"));
            Assert.AreEqual(2, this.logic.ChampionshipViewModel.GetPoints().Count());
            Assert.AreEqual(true, this.logic.ChampionshipViewModel.GetPoints().Contains(25));
            Assert.AreEqual(true, this.logic.ChampionshipViewModel.GetPoints().Contains(18));
        }

        /// <summary>
        /// Tests GetLapTimePrediction method.
        /// </summary>
        [Test]
        public void GetLapTimePrediction()
        {
            this.logic.GetLapTimePrediction(1);

            Assert.AreEqual(3, this.logic.LapTimePredictionViewModel.LapTimePredictions.Count());
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.LapTimePredictions.ToArray()[0].Lap);
            Assert.AreEqual(2, this.logic.LapTimePredictionViewModel.LapTimePredictions.ToArray()[1].Lap);
            Assert.AreEqual(3, this.logic.LapTimePredictionViewModel.LapTimePredictions.ToArray()[2].Lap);
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.Circuits.Count());
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.Circuits.First().CircuitID);
            Assert.AreEqual("Szolnok City Circuit", this.logic.LapTimePredictionViewModel.Circuits.First().Name);
            Assert.AreEqual("Szolnok", this.logic.LapTimePredictionViewModel.Circuits.First().Location);
            Assert.AreEqual("Hungary", this.logic.LapTimePredictionViewModel.Circuits.First().Country);
            Assert.AreEqual(6, this.logic.LapTimePredictionViewModel.LapTimes.Count());
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.LapTimes.First().Lap);
            Assert.AreEqual(60000, this.logic.LapTimePredictionViewModel.LapTimes.First().Milliseconds);
            Assert.AreEqual(2, this.logic.LapTimePredictionViewModel.LapTimes.First().RaceID);
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.LapTimes.First().DriverID);
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.SelectedCircuitID);
            Assert.AreEqual("1:00.0", this.logic.LapTimePredictionViewModel.GetTimesForAxis().First());
            Assert.AreEqual(this.logic.LapTimePredictionViewModel.LapTimePredictions.First().Milliseconds, this.logic.LapTimePredictionViewModel.GetLapTimePredictions().First());
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.GetLapTimePredictionLaps().First());
            Assert.AreEqual(60000, this.logic.LapTimePredictionViewModel.GetMilliseconds().First());
            Assert.AreEqual(1, this.logic.LapTimePredictionViewModel.GetLaps().First());
        }

        /// <summary>
        /// Tests CalculateFailures method.
        /// </summary>
        [Test]
        public void GetFailures()
        {
            this.logic.CalculateFailures(2022);

            Assert.AreEqual(0, this.logic.FailureViewModel.Failures.Count());
            Assert.AreEqual(2022, this.logic.FailureViewModel.SelectedYear);
            Assert.AreEqual(2, this.logic.FailureViewModel.Years.Count());
            Assert.AreEqual(2022, this.logic.FailureViewModel.Years.First());
            Assert.AreEqual(2021, this.logic.FailureViewModel.Years.Last());
        }
    }
}
