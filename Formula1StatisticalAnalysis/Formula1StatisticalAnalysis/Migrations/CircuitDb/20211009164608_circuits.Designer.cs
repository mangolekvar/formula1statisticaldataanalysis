﻿// <auto-generated />
using Formula1StatisticalAnalysis.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Formula1StatisticalAnalysis.Migrations.CircuitDb
{
    [DbContext(typeof(CircuitDbContext))]
    [Migration("20211009164608_circuits")]
    partial class Circuits
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.10")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Formula1StatisticalAnalysis.Models.Circuit", b =>
                {
                    b.Property<int>("CircuitID")
                        .HasColumnType("int");

                    b.Property<string>("Country")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Location")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("CircuitID");

                    b.ToTable("Circuits");
                });
#pragma warning restore 612, 618
        }
    }
}
