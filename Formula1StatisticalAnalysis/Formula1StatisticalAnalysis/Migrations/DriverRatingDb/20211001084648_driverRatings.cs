﻿// <copyright file="20211001084648_driverRatings.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Migrations.DriverRatingDb
{
    using Microsoft.EntityFrameworkCore.Migrations;

    /// <summary>
    /// Driver Rating migration class.
    /// </summary>
    public partial class DriverRatings : Migration
    {
        /// <summary>
        /// Migration method for driver ratings.
        /// </summary>
        /// <param name="migrationBuilder">Migration builder.</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DriverRatings",
                columns: table => new
                {
                    DriverID = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AvgFinPos = table.Column<double>(type: "float", nullable: false),
                    NORaces = table.Column<int>(type: "int", nullable: false),
                    AvgFinPosVsTM = table.Column<double>(type: "float", nullable: false),
                    AvgFinTimeVsTM = table.Column<double>(type: "float", nullable: false),
                    FinPosVSNOPart = table.Column<double>(type: "float", nullable: false),
                    TMRating = table.Column<double>(type: "float", nullable: false),
                    DriverRating = table.Column<double>(type: "float", nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverRatings", x => x.DriverID);
                });
        }

        /// <summary>
        /// Drops driver rating table.
        /// </summary>
        /// <param name="migrationBuilder">Migration builder.</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DriverRatings");
        }
    }
}
