﻿// <copyright file="20210920144540_laptime.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Migrations.LapTimeDb
{
    using Microsoft.EntityFrameworkCore.Migrations;

    /// <summary>
    /// Lap time class.
    /// </summary>
    public partial class Laptime : Migration
    {
        /// <summary>
        /// Creates table.
        /// </summary>
        /// <param name="migrationBuilder">Migration builder.</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LapTimes",
                columns: table => new
                {
                    RaceID = table.Column<int>(type: "int", nullable: false),
                    DriverID = table.Column<int>(type: "int", nullable: false),
                    Lap = table.Column<int>(type: "int", nullable: false),
                    Milliseconds = table.Column<int>(type: "int", nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LapTimes", x => new { x.RaceID, x.DriverID, x.Lap });
                });
        }

        /// <summary>
        /// Drops table.
        /// </summary>
        /// <param name="migrationBuilder">Migration builder.</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LapTimes");
        }
    }
}
