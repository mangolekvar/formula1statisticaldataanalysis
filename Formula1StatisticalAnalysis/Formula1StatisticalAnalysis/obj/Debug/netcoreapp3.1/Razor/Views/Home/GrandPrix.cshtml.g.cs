#pragma checksum "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "60d74804f76f43f2c58a9b5631831fa3903f6567"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_GrandPrix), @"mvc.1.0.view", @"/Views/Home/GrandPrix.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
using Formula1StatisticalAnalysis.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
using Formula1StatisticalAnalysis.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
using System.Web.Helpers;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"60d74804f76f43f2c58a9b5631831fa3903f6567", @"/Views/Home/GrandPrix.cshtml")]
    public class Views_Home_GrandPrix : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<GrandPrixVM>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<ul class=""nav nav-tabs"">
    <li class=""nav-item"">
        <a class=""nav-link"" href=""/home/Index"">Home</a>
    </li>
    <li class=""nav-item"">
        <a class=""nav-link"" href=""/home/Championship"">Championships in dept</a>
    </li>
    <li class=""nav-item"" aria-current=""page"">
        <a class=""nav-link active"" aria-current=""page"" href=""/home/GrandPrix"">Races in dept</a>
    </li>
    <li class=""nav-item"">
        <a class=""nav-link"" href=""/home/DriverRatings"">Driver Ratings</a>
    </li>
    <li class=""nav-item"">
        <a class=""nav-link"" href=""/home/LapTimePrediction"">Lap time prediction</a>
    </li>
    <li class=""nav-item"">
        <a class=""nav-link"" href=""/home/Failures"">Causing of DNFs</a>
    </li>
</ul>
<h1></h1>
<label class=""form-label"" style=""font-size: 25px"">
    In this page you can look further into Formula 1 races by selecting the year and the round number (these will identify the race itself) and then selecting a range of laps which you want to investigate more. On ");
            WriteLiteral(@"the visualization below you will see the result of the drivers by how fast they completed the selected laps, and how much time they were slower than the driver who completed it the fastest. Below the visualization you can see the final data in table as well.
</label>
<form method=""post"" action=""/Home/GrandPrix"">
    <div class=""form-group"">
        <h1></h1>
        <label for=""yearSelection"" class=""form-label"">Select year</label>
        <select name=""year"" class=""form-select"" aria-label=""select example"" onchange=""getRounds(); getLaps();"" id=""yearSelection"">
");
#nullable restore
#line 34 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
             foreach (int year in Model.Years)
            {
                if (year != Model.SelectedYear)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <option");
            BeginWriteAttribute("value", " value=\"", 1892, "\"", 1905, 1);
#nullable restore
#line 38 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 1900, year, 1900, 5, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 38 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                     Write(year);

#line default
#line hidden
#nullable disable
            WriteLiteral("</option>\r\n");
#nullable restore
#line 39 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <option selected");
            BeginWriteAttribute("value", " value=\"", 2019, "\"", 2046, 1);
#nullable restore
#line 42 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 2027, Model.SelectedYear, 2027, 19, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 42 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                            Write(Model.SelectedYear);

#line default
#line hidden
#nullable disable
            WriteLiteral("</option>\r\n");
#nullable restore
#line 43 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                }
            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"        </select>
    </div>
    <div class=""form-group"">
        <h1></h1>
        <label for=""roundSelection"" class=""form-label"">Select round</label>
        <select name=""round"" class=""form-select"" aria-label=""select example"" id=""roundSelection"" onchange=""getLaps()"">
");
#nullable restore
#line 51 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
             foreach (GrandPrixInfo gpInfo in Model.GrandPrixInfosByRaces)
            {
                if (gpInfo.Year == Model.SelectedYear)
                {
                    if (gpInfo.Round != Model.SelectedRound)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <option");
            BeginWriteAttribute("value", " value=\"", 2671, "\"", 2692, 1);
#nullable restore
#line 57 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 2679, gpInfo.Round, 2679, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">[Round ");
#nullable restore
#line 57 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                        Write(gpInfo.Round);

#line default
#line hidden
#nullable disable
            WriteLiteral("] ");
#nullable restore
#line 57 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                                       Write(gpInfo.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</option>\r\n");
#nullable restore
#line 58 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <option selected");
            BeginWriteAttribute("value", " value=\"", 2851, "\"", 2872, 1);
#nullable restore
#line 61 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 2859, gpInfo.Round, 2859, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">[Round ");
#nullable restore
#line 61 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                                 Write(gpInfo.Round);

#line default
#line hidden
#nullable disable
            WriteLiteral("] ");
#nullable restore
#line 61 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                                                Write(gpInfo.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</option>\r\n");
#nullable restore
#line 62 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                    }
                }
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </select>\r\n    </div>\r\n    <div class=\" form-group\">\r\n        <h1></h1>\r\n        <label for=\"lapSelection\" class=\"form-label\">Select the range of laps: <p id=\"selectedLaps\" style=\"display:inline\"></p></label>\r\n");
#nullable restore
#line 70 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
          
            int i = 0;
            while (Model.GrandPrixInfosByRaces.ToArray()[i].Year != Model.SelectedYear || Model.GrandPrixInfosByRaces.ToArray()[i].Round != Model.SelectedRound)
            {
                i++;
            }
        

#line default
#line hidden
#nullable disable
#nullable restore
#line 77 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
         if (Model.SelectedLaps.Contains(0))
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <input type=\"range\" name=\"laps1\" class=\"form-range\" value=\"1\" min=\"1\"");
            BeginWriteAttribute("max", " max=\"", 3593, "\"", 3647, 1);
#nullable restore
#line 79 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 3599, Model.GrandPrixInfosByRaces.ToArray()[i].NOLaps, 3599, 48, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" id=\"Laps1\">\r\n            <input type=\"range\" name=\"laps2\" class=\"form-range\"");
            BeginWriteAttribute("value", " value=\"", 3725, "\"", 3781, 1);
#nullable restore
#line 80 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 3733, Model.GrandPrixInfosByRaces.ToArray()[i].NOLaps, 3733, 48, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" min=\"1\"");
            BeginWriteAttribute("max", " max=\"", 3790, "\"", 3844, 1);
#nullable restore
#line 80 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 3796, Model.GrandPrixInfosByRaces.ToArray()[i].NOLaps, 3796, 48, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" id=\"Laps2\">\r\n");
#nullable restore
#line 81 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
        }
        else
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <input type=\"range\" name=\"laps1\" class=\"form-range\"");
            BeginWriteAttribute("value", " value=\"", 3958, "\"", 3993, 1);
#nullable restore
#line 84 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 3966, Model.SelectedLaps.First(), 3966, 27, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" min=\"1\"");
            BeginWriteAttribute("max", " max=\"", 4002, "\"", 4056, 1);
#nullable restore
#line 84 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 4008, Model.GrandPrixInfosByRaces.ToArray()[i].NOLaps, 4008, 48, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" id=\"Laps1\">\r\n            <input type=\"range\" name=\"laps2\" class=\"form-range\"");
            BeginWriteAttribute("value", " value=\"", 4134, "\"", 4168, 1);
#nullable restore
#line 85 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 4142, Model.SelectedLaps.Last(), 4142, 26, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" min=\"1\"");
            BeginWriteAttribute("max", " max=\"", 4177, "\"", 4231, 1);
#nullable restore
#line 85 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
WriteAttributeValue("", 4183, Model.GrandPrixInfosByRaces.ToArray()[i].NOLaps, 4183, 48, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" id=\"Laps2\">\r\n");
#nullable restore
#line 86 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"    </div>
    <input type=""submit"" id=""btn_click"" class=""btn btn-primary"" value=""Select"" />
</form>
<h1></h1>
<div id=""label"" style=""text-align: center;"">
    <b>
        <label for=""viz"" class=""form-label"">Pace of drivers: </label>
        <label for=""viz"" class=""form-label"">");
#nullable restore
#line 94 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                       Write(Model.SelectedYear);

#line default
#line hidden
#nullable disable
            WriteLiteral(",</label>\r\n");
#nullable restore
#line 95 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
         foreach (GrandPrixInfo gpInfo in Model.GrandPrixInfosByRaces)
        {
            if (gpInfo.Year == Model.SelectedYear && gpInfo.Round == Model.SelectedRound)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <label for=\"viz\" class=\"form-label\">");
#nullable restore
#line 99 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                               Write(gpInfo.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(" (Round ");
#nullable restore
#line 99 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                                   Write(gpInfo.Round);

#line default
#line hidden
#nullable disable
            WriteLiteral("), </label>\r\n");
#nullable restore
#line 100 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
            }
        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 102 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
         if (Model.SelectedLaps.Contains(0))
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <label for=\"viz\" class=\"form-label\">from lap 1 to ");
#nullable restore
#line 104 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                         Write(Model.GrandPrixInfosByRaces.ToArray()[i].NOLaps);

#line default
#line hidden
#nullable disable
            WriteLiteral("</label>\r\n");
#nullable restore
#line 105 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
        }
        else
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <label for=\"viz\" class=\"form-label\">from lap ");
#nullable restore
#line 108 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                    Write(Model.SelectedLaps.First());

#line default
#line hidden
#nullable disable
            WriteLiteral(" to ");
#nullable restore
#line 108 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                                                   Write(Model.SelectedLaps.Last());

#line default
#line hidden
#nullable disable
            WriteLiteral("</label>\r\n");
#nullable restore
#line 109 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </b>\r\n</div>\r\n<div id=\"viz\"></div>\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
#nullable restore
#line 117 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
           Write(Html.DisplayNameFor(model => model.GrandPrixes.First().DriverName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 120 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
           Write(Html.DisplayNameFor(model => model.GrandPrixes.First().Time));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                GapToLeader\r\n            </th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 128 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
         foreach (GrandPrix gp in Model.GrandPrixes)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td class=\"GPdriverName\">\r\n                    <span class=\"name\">");
#nullable restore
#line 132 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                  Write(Html.DisplayFor(modelItem => gp.DriverName));

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n                </td>\r\n                <td class=\"GPtime\">\r\n                    ");
#nullable restore
#line 135 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
               Write(Html.DisplayFor(modelItem => gp.Time));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td class=\"GPgapToWinner\">\r\n                    ");
#nullable restore
#line 138 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
               Write(Html.DisplayFor(modelItem => gp.GapToWinner));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n");
#nullable restore
#line 141 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 142 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
         if (Model.GrandPrixes.Count() == 0)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>\r\n                    <label for=\"customRange2\" class=\"form-label\">Sorry, there is no data for Year: ");
#nullable restore
#line 146 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                                                              Write(Model.SelectedYear);

#line default
#line hidden
#nullable disable
            WriteLiteral(", Round: ");
#nullable restore
#line 146 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                                                                                                                          Write(Model.SelectedRound);

#line default
#line hidden
#nullable disable
            WriteLiteral(" in this database. :(</label>\r\n                </td>\r\n            </tr>\r\n");
#nullable restore
#line 149 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>\r\n<script>\r\n    var pointerX = -1;\r\n    var pointerY = -1;\r\n    document.addEventListener(\"mousemove\", () => {\r\n        pointerX = event.clientX;\r\n        pointerY = event.clientY;\r\n    });\r\n");
#nullable restore
#line 159 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
      
        List<int> infoYears = new List<int>();
        List<int> infoRounds = new List<int>();
        List<string> infoNames = new List<string>();
        List<int> infoLaps = new List<int>();
        foreach (GrandPrixInfo gpInfo in Model.GrandPrixInfosByRaces)
        {
            infoYears.Add(gpInfo.Year);
            infoRounds.Add(gpInfo.Round);
            infoNames.Add(gpInfo.Name);
            infoLaps.Add(gpInfo.NOLaps);
        }

    

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    var infoYears = ");
#nullable restore
#line 174 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
               Write(Html.Raw(Json.Serialize(infoYears)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n    var infoRounds = ");
#nullable restore
#line 175 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                Write(Html.Raw(Json.Serialize(infoRounds)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n    var infoLaps = ");
#nullable restore
#line 176 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
              Write(Html.Raw(Json.Serialize(infoLaps)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n    var infoNames = ");
#nullable restore
#line 177 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
               Write(Html.Raw(Json.Serialize(infoNames)));

#line default
#line hidden
#nullable disable
            WriteLiteral(@";
    var infoData = [];
    for (var i = 0; i < infoYears.length; i++) {
        infoData.push({ year: infoYears[i], round: infoRounds[i], name: infoNames[i], noLaps: infoLaps[i] });
    }
    function getRounds() {
        var selectedYear = document.getElementById(""yearSelection"").value;
        var selectedRound = document.getElementById(""roundSelection"").value;
        var str = """";
        for (var i = 0; i < infoData.length; i++) {
            if (infoData[i].year == selectedYear) {
                if (infoData[i].round != selectedRound) {
                    str += ""<option value=\"""" + infoData[i].round + ""\"">[Round "" + infoData[i].round + ""] "" + infoData[i].name + ""</option>""
                }
                else {
                    str += ""<option selected value=\"""" + infoData[i].round + ""\"">[Round "" + infoData[i].round + ""] "" + infoData[i].name + ""</option>""
                }
            }
        }
        document.getElementById(""roundSelection"").innerHTML = str;
    }

 ");
            WriteLiteral(@"   function getLaps() {
        var selectedYear = document.getElementById(""yearSelection"").value;
        var selectedRound = document.getElementById(""roundSelection"").value;
        for (var i = 0; i < infoData.length; i++) {
            if (infoData[i].year == selectedYear && infoData[i].round == selectedRound) {
                document.getElementById(""Laps1"").max = infoData[i].noLaps;
                document.getElementById(""Laps2"").max = infoData[i].noLaps;
                if (+document.getElementById(""Laps1"").value > infoData[i].noLaps) {
                    document.getElementById(""Laps1"").value = infoData[i].noLaps;
                }
                if (+document.getElementById(""Laps2"").value > infoData[i].noLaps) {
                    document.getElementById(""Laps2"").value = infoData[i].noLaps;
                }
                if (+document.getElementById(""Laps2"").value < +document.getElementById(""Laps1"").value) {
                    document.getElementById(""selectedLaps"").innerHTML =");
            WriteLiteral(@" document.getElementById(""Laps2"").value + "" - "" + document.getElementById(""Laps1"").value;
                }
                else {
                    document.getElementById(""selectedLaps"").innerHTML = document.getElementById(""Laps1"").value + "" - "" + document.getElementById(""Laps2"").value;
                }
            }
        }
    }

    function getLapsStart() {
        var selectedYear = ");
#nullable restore
#line 223 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                      Write(Html.Raw(Json.Serialize(Model.SelectedYear)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n        var selectedRound = ");
#nullable restore
#line 224 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
                       Write(Html.Raw(Json.Serialize(Model.SelectedRound)));

#line default
#line hidden
#nullable disable
            WriteLiteral(@";
        for (var i = 0; i < infoData.length; i++) {
            if (infoData[i].year == selectedYear && infoData[i].round == selectedRound) {
                document.getElementById(""Laps1"").max = infoData[i].noLaps;
                document.getElementById(""Laps2"").max = infoData[i].noLaps;
                if (+document.getElementById(""Laps1"").value > infoData[i].noLaps) {
                    document.getElementById(""Laps1"").value = infoData[i].noLaps;
                }
                if (+document.getElementById(""Laps2"").value > infoData[i].noLaps) {
                    document.getElementById(""Laps2"").value = infoData[i].noLaps;
                }
                if (+document.getElementById(""Laps2"").value < +document.getElementById(""Laps1"").value) {
                    document.getElementById(""selectedLaps"").innerHTML = document.getElementById(""Laps2"").value + "" - "" + document.getElementById(""Laps1"").value;
                }
                else {
                    document.getElementByI");
            WriteLiteral(@"d(""selectedLaps"").innerHTML = document.getElementById(""Laps1"").value + "" - "" + document.getElementById(""Laps2"").value;
                }
            }
        }
    }

    if (+document.getElementById(""Laps2"").value < +document.getElementById(""Laps1"").value) {
        document.getElementById(""selectedLaps"").innerHTML = document.getElementById(""Laps2"").value + "" - "" + document.getElementById(""Laps1"").value;
    }
    else {
        document.getElementById(""selectedLaps"").innerHTML = document.getElementById(""Laps1"").value + "" - "" + document.getElementById(""Laps2"").value;
    }

    document.getElementById(""Laps1"").oninput = function () {

        if (+document.getElementById(""Laps2"").value < +document.getElementById(""Laps1"").value) {
            document.getElementById(""selectedLaps"").innerHTML = document.getElementById(""Laps2"").value + "" - "" + document.getElementById(""Laps1"").value;
        }
        else {
            document.getElementById(""selectedLaps"").innerHTML = document.getElementBy");
            WriteLiteral(@"Id(""Laps1"").value + "" - "" + document.getElementById(""Laps2"").value;
        }
    }

    document.getElementById(""Laps2"").oninput = function () {

        if (+document.getElementById(""Laps2"").value < +document.getElementById(""Laps1"").value) {
            document.getElementById(""selectedLaps"").innerHTML = document.getElementById(""Laps2"").value + "" - "" + document.getElementById(""Laps1"").value;
        }
        else {
            document.getElementById(""selectedLaps"").innerHTML = document.getElementById(""Laps1"").value + "" - "" + document.getElementById(""Laps2"").value;
        }
    }
");
#nullable restore
#line 271 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
      
        double[] times = Model.GetTimes();
        string[] names = Model.GetDriverNames();
        double[] gaps = Model.GetGaps();
    

#line default
#line hidden
#nullable disable
            WriteLiteral("    var names = ");
#nullable restore
#line 276 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
           Write(Html.Raw(Json.Serialize(names)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n    var data = ");
#nullable restore
#line 277 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
          Write(Html.Raw(Json.Serialize(times)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n    var gaps = ");
#nullable restore
#line 278 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
          Write(Html.Raw(Json.Serialize(gaps)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n    var bardata = [];\r\n    for (var i = 0; i < data.length; i++) {\r\n        bardata.push({ index: i, value: data[i], name: names[names.length - i - 1], gap: gaps[i]});\r\n    }\r\n    var names = ");
#nullable restore
#line 283 "D:\OE\7. félév\SZAKDOLGOZAT\Back-end\Formula1StatisticalAnalysis\Formula1StatisticalAnalysis\Views\Home\GrandPrix.cshtml"
           Write(Html.Raw(Json.Serialize(names)));

#line default
#line hidden
#nullable disable
            WriteLiteral(@";
    var margin = { top: 20, right: 0, bottom: 20, left: 100 },
        height = 600 - margin.top - margin.bottom,
        width = 1300 - margin.left - margin.right,
        barWidth = 50,
        barOffset = 25;

    var xScale = d3.scaleLinear().domain([d3.max(data) / (1.5087732342 - (0.0000000743218135 * d3.max(data))), d3.max(data)]).range([0, width]);

    function smallerThan10(number) {
        return (number < 10 ? '0' + number : number);
    }

    function smallerThan100(number) {
        return (number < 100 ? (number < 10 ? '00' + number : '0' + number) : number);
    }

    function TimeConverter(number) {
        if (Number(number) == 0) {
            return '0';
        }
        else if (Number(number) < 1000) { // smaller than a second
            return '0.' + smallerThan100(number);
        }
        else if (Number(number) < 60000) { // smaller than a minute
            return Math.floor(number / 1000) + '.' + smallerThan100(number % 1000);
        }
        els");
            WriteLiteral(@"e if (Number(number) < 3600000) { // smaller than an hour
            return Math.floor(number / 60000) + ':' + smallerThan10(Math.floor(number / 1000) % 60) + '.' + smallerThan100(number % 1000);
        }
        else { // bigger than an hour
            return Math.floor(number / 3600000) + ':' + smallerThan10(Math.floor(number / 60000) % 60) + ':' + smallerThan10(Math.floor(number / 1000) % 60) + '.' + smallerThan100(number % 1000);
        }
    }

    var xAxisValues = d3.scaleLinear().domain([d3.max(data) / (1.5087732342 - (0.0000000743218135 * d3.max(data))), d3.max(data)]).range([width, 0]);
    var xAxisTicks = d3.axisBottom(xScale).ticks(10).tickFormat((d) => TimeConverter(d));

    var yAxisValues = d3.scaleBand().domain(d3.range(names.length)).rangeRound([height, 0]).padding(0.1);
    var yAxisTicks = d3.axisLeft(yAxisValues).tickFormat((i) => names[i]);

    var yScale = d3.scaleBand().domain(bardata).paddingInner(.3).paddingOuter(.3).range([0, height])

    var colors = d3.scale");
            WriteLiteral(@"Linear()
        .domain([0, bardata.length * .33, bardata.length * .66, bardata.length])
        .range(['#b58929', '#c61c6f', '#268bd2', '#85992c']);

    var tooltip = d3.select('#viz')
        .append('div')
        .style('opacity', '0')
        .style('background-color', 'white')
        .style('border', 'solid')
        .style('border-width', '2px')
        .style('border-radius', '5px')
        .style('padding', '5px')
        .style('position', 'absolute');

    var myChart = d3
        .select('#viz').append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.right + ')')
        .selectAll('rect').data(bardata)
        .enter().append('rect')
        .style('fill', function (d, i) {
            return colors(i)
        })
        .attr('height', function (d) {
            return yScale.bandwidth();
        })");
            WriteLiteral(@"
        .attr('y', function (d) {
            return yScale(d);
        })
        .attr('width', 0)
        .attr('x', 0)
        .on('mouseover', function (event, d) {
            tooltip
                .style(""opacity"", 1)
                .style(""left"", (pointerX + document.documentElement.scrollLeft + 10) + ""px"")
                .style(""top"", (pointerY + document.documentElement.scrollTop + 10) + ""px"")
        })
        .on('mousemove', function (event, d) {
            tooltip
                .html((d.index + 1) + "". "" + d.name + ""</br>Gap to leader: "" + TimeConverter(d.gap))
                .style(""left"", (pointerX + document.documentElement.scrollLeft + 10) + ""px"")
                .style(""top"", (pointerY + document.documentElement.scrollTop + 10) + ""px"")
        })
        .on('mouseleave', function (d) {
            tooltip
                .style(""opacity"", 0)
        });

     var xGuide = d3.select('#viz svg').append('g').attr('transform', 'translate(' + margin.left + ',' + ");
            WriteLiteral(@"height + ')')
         .call(xAxisTicks)
    var yGuide = d3.select('#viz svg').append('g').attr('transform', 'translate(' + margin.left + ',0)')
        .call(yAxisTicks)

    myChart.transition()
        .attr('width', function (d) {
            return xScale(d.value);
        })
        .delay(function (d, i) {
            return i * 10;
        })
        .duration(1000)
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<GrandPrixVM> Html { get; private set; }
    }
}
#pragma warning restore 1591
