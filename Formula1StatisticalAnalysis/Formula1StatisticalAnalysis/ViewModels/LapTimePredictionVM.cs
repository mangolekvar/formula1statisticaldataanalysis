﻿// <copyright file="LapTimePredictionVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// View model class for lap time prediction.
    /// </summary>
    public class LapTimePredictionVM
    {
        /// <summary>
        /// Gets or sets collection of lap time predictions.
        /// </summary>
        public IEnumerable<LapTimePrediction> LapTimePredictions { get; set; }

        /// <summary>
        /// Gets or sets available circuits.
        /// </summary>
        public IList<Circuit> Circuits { get; set; }

        /// <summary>
        /// Gets or sets collection of lap times.
        /// </summary>
        public IEnumerable<LapTime> LapTimes { get; set; }

        /// <summary>
        /// Gets or sets selected circuit ID.
        /// </summary>
        public int SelectedCircuitID { get; set; }

        /// <summary>
        /// Gets the lap times in nice form from the collection.
        /// </summary>
        /// <returns>Array of lap times.</returns>
        public string[] GetTimesForAxis()
        {
            string[] times = new string[this.LapTimes.Count()];
            int i = this.LapTimes.Count() - 1;
            foreach (var lp in this.LapTimes)
            {
                if (((lp.Milliseconds / 1000) / 60) < 1)
                {
                    if (((lp.Milliseconds / 1000) % 60) < 10)
                    {
                        times[i--] = "0:0" + (int)((lp.Milliseconds / 1000) % 60) + "." + (int)(lp.Milliseconds % 1000);
                    }
                    else
                    {
                        times[i--] = "0:" + (int)((lp.Milliseconds / 1000) % 60) + "." + (int)(lp.Milliseconds % 1000);
                    }
                }
                else
                {
                    if (((lp.Milliseconds / 1000) % 60) < 10)
                    {
                        times[i--] = (int)((lp.Milliseconds / 1000) / 60) + ":0" + (int)((lp.Milliseconds / 1000) % 60) + "." + (int)(lp.Milliseconds % 1000);
                    }
                    else
                    {
                        times[i--] = (int)((lp.Milliseconds / 1000) / 60) + ":" + (int)((lp.Milliseconds / 1000) % 60) + "." + (int)(lp.Milliseconds % 1000);
                    }
                }
            }

            return times;
        }

        /// <summary>
        /// Gets lap time predicitons from the collection.
        /// </summary
        /// <returns>Array of times in milliseconds.</returns>
        public double[] GetLapTimePredictions()
        {
            double[] millis = new double[this.LapTimePredictions.Count()];
            int i = 0;
            foreach (var lp in this.LapTimePredictions)
            {
                millis[i++] = lp.Milliseconds;
            }

            return millis;
        }

        /// <summary>
        /// Gets lap of lap time predictions from the collection.
        /// </summary
        /// <returns>Array of times in milliseconds.</returns>
        public int[] GetLapTimePredictionLaps()
        {
            int[] millis = new int[this.LapTimePredictions.Count()];
            int i = 0;
            foreach (var lp in this.LapTimePredictions)
            {
                millis[i++] = lp.Lap;
            }

            return millis;
        }

        /// <summary>
        /// Gets times in milliseconds from the collection.
        /// </summary
        /// <returns>Array of times in milliseconds.</returns>
        public double[] GetMilliseconds()
        {
            double[] millis = new double[this.LapTimes.Count()];
            int i = 0;
            foreach (var lp in this.LapTimes)
            {
                millis[i++] = lp.Milliseconds;
            }

            return millis;
        }

        /// <summary>
        /// Gets laps from the collection.
        /// </summary
        /// <returns>Array of laps.</returns>
        public double[] GetLaps()
        {
            double[] laps = new double[this.LapTimes.Count()];
            int i = 0;
            foreach (var lp in this.LapTimes)
            {
                laps[i++] = lp.Lap;
            }

            return laps;
        }
    }
}
