﻿// <copyright file="GrandPrixVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Helpers;
    using Formula1StatisticalAnalysis.Data;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// ViewModel class for Grand Prix view.
    /// </summary>
    public class GrandPrixVM
    {
        /// <summary>
        /// Gets or sets collection of grand prixes.
        /// </summary>
        public IEnumerable<GrandPrix> GrandPrixes { get; set; }

        /// <summary>
        /// Gets or sets Available years.
        /// </summary>
        public IList<int> Years { get; set; }

        /// <summary>
        /// Gets or sets Available rounds.
        /// </summary>
        public IList<int> Rounds { get; set; }

        /// <summary>
        /// Gets or sets selected year.
        /// </summary>
        public int SelectedYear { get; set; }

        /// <summary>
        /// Gets or sets selected round.
        /// </summary>
        public int SelectedRound { get; set; }

        /// <summary>
        /// Gets or sets selected laps.
        /// </summary>
        public IList<int> SelectedLaps { get; set; }

        /// <summary>
        /// Gets or sets grand prix info by races.
        /// </summary>
        public IList<GrandPrixInfo> GrandPrixInfosByRaces { get; set; }

        /// <summary>
        /// Gets the drivers' name from the collection.
        /// </summary>
        /// <returns>Array of drivers' name.</returns>
        public string[] GetDriverNames()
        {
            string[] names = new string[this.GrandPrixes.Count()];
            int i = this.GrandPrixes.Count() - 1;
            foreach (var gp in this.GrandPrixes)
            {
                names[i--] = gp.DriverName;
            }

            return names;
        }

        /// <summary>
        /// Gets one driver's time from the collection.
        /// </summary
        /// <returns>Driver's time.</returns>
        public double[] GetTimes()
        {
            double[] times = new double[this.GrandPrixes.Count()];
            int i = 0;
            foreach (var gp in this.GrandPrixes)
            {
                times[i++] = gp.Time;
            }

            return times;
        }

        /// <summary>
        /// Gets one driver's gap to winner from the collection.
        /// </summary
        /// <returns>Driver's time.</returns>
        public double[] GetGaps()
        {
            double[] gaps = new double[this.GrandPrixes.Count()];
            int i = 0;
            foreach (var gp in this.GrandPrixes)
            {
                gaps[i++] = gp.GapToWinner;
            }

            return gaps;
        }
    }
}
