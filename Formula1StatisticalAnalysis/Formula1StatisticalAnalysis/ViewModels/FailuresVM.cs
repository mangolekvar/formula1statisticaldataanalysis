﻿// <copyright file="FailuresVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// View model class for Failures.
    /// </summary>
    public class FailuresVM
    {
        /// <summary>
        /// Gets or sets Collection of failures.
        /// </summary>
        public IEnumerable<Failure> Failures { get; set; }

        /// <summary>
        /// Gets or sets Selected year.
        /// </summary>
        public int SelectedYear { get; set; }

        /// <summary>
        /// Gets or sets List of years.
        /// </summary>
        public IList<int> Years { get; set; }
    }
}
