﻿// <copyright file="ChampionshipVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Championship view model class.
    /// </summary>
    public class ChampionshipVM
    {
        /// <summary>
        /// Gets or sets collection of championship data.
        /// </summary>
        public IEnumerable<Championship> Championships { get; set; }

        /// <summary>
        /// Gets or sets Available years.
        /// </summary>
        public IList<int> Years { get; set; }

        /// <summary>
        /// Gets or sets Available rounds.
        /// </summary>
        public IList<int> RoundsByYears { get; set; }

        /// <summary>
        /// Gets or sets selected year.
        /// </summary>
        public int SelectedYear { get; set; }

        /// <summary>
        /// Gets or sets selected rounds.
        /// </summary>
        public IList<int> SelectedRounds { get; set; }

        /// <summary>
        /// Gets the drivers' name from the collection.
        /// </summary>
        /// <returns>Array of drivers' name.</returns>
        public string[] GetDriverNames()
        {
            string[] names = new string[this.Championships.Count()];
            int i = this.Championships.Count() - 1;
            foreach (var ch in this.Championships)
            {
                names[i--] = ch.DriverName;
            }

            return names;
        }

        /// <summary>
        /// Gets one driver's points from the collection.
        /// </summary
        /// <returns>Driver's points.</returns>
        public double[] GetPoints()
        {
            double[] points = new double[this.Championships.Count()];
            int i = 0;
            foreach (var ch in this.Championships)
            {
                points[i++] = ch.Points;
            }

            return points;
        }
    }
}
