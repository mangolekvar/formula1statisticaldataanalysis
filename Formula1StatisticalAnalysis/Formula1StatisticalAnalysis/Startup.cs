// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Data;
    using FsKaggle.Interop;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Data.SqlClient;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// Startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Downloads all the datasets and configure services method.
        /// </summary>
        /// <param name="services">Collection of services.</param>
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            this.DownloadDatasets();

            services.AddTransient<IStatusRepository, StatusSQLRepository>();
            services.AddTransient<ICircuitRepository, CircuitSQLRepository>();
            services.AddTransient<IRaceRepository, RaceSQLRepository>();
            services.AddTransient<IDriverRepository, DriverSQLRepository>();
            services.AddTransient<IResultRepository, ResultSQLRepository>();
            services.AddTransient<ILapTimeRepository, LapTimeSQLRepository>();
            services.AddTransient<IDriverRatingRepository, DriverRatingSQLRepository>();
            services.AddTransient<IFailureRepository, FailureSQLRepository>();

            services.AddDbContext<FailureDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=FailureDB,Trusted_Connection=True;");
            });

            services.AddDbContext<DriverRatingDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=DriverRatingDB,Trusted_Connection=True;");
            });

            services.AddDbContext<StatusDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=StatusDB,Trusted_Connection=True;");
            });

            services.AddDbContext<CircuitDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server = (localdb)\mssqllocaldb; Database = CircuitDB,Trusted_Connection = True;");
            });
            services.AddDbContext<RaceDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=RaceDB,Trusted_Connection=True;");
            });
            services.AddDbContext<DriverDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=DriverDB,Trusted_Connection=True;");
            });
            services.AddDbContext<ResultDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ResultDB,Trusted_Connection=True;");
            });
            services.AddDbContext<LapTimeDbContext>(opt =>
            {
                opt.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=LapTimeDB,Trusted_Connection=True;");
            });
            services.AddMvc(opt => opt.EnableEndpointRouting = false).AddRazorRuntimeCompilation();
        }

        /// <summary>
        /// Configure method.
        /// </summary>
        /// <param name="app">Application builder.</param>
        /// <param name="env">Webhost environment.</param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvcWithDefaultRoute();

            app.UseStaticFiles();

            app.UseRouting();
        }

        private void DownloadDatasets()
        {
            this.DownloadDataset("circuits");
            this.DownloadDataset("drivers");
            this.DownloadDataset("lap_times");
            this.DownloadDataset("races");
            this.DownloadDataset("results");
            this.DownloadDataset("status");
        }

        /// <summary>
        /// Downloads a dataset from <see href="/>www.kaggle.com/rohanrao/formula-1-world-championship-1950-2020"/> kaggle.
        /// </summary>
        /// <param name="fileName">Name of the dataset to download.</param>
        private async void DownloadDataset(string fileName)
        {
            var options =
                    new DatasetInfo
                    {
                        Owner = "rohanrao",
                        Dataset = "formula-1-world-championship-1950-2020",
                        Request = fileName + ".csv",
                    };

            var advancedOptions = new DownloadDatasetOptions
            {
                DatasetInfo = options,
                KaggleJsonPath = "kaggle.json",
                DestinationFolder = "Datasets",
                ReportingCallback = FsKaggle.Reporter.ProgressBar,
                Overwrite = true,
            };

            await Kaggle.DownloadDatasetAsync(advancedOptions);
        }
    }
}
