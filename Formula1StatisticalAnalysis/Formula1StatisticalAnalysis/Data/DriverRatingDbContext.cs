﻿// <copyright file="DriverRatingDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Driver rating database context class.
    /// </summary>
    public class DriverRatingDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DriverRatingDbContext"/> class.
        /// </summary>
        /// <param name="opt">Database context option to the base class.</param>
        public DriverRatingDbContext(DbContextOptions<DriverRatingDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets the Driver ratings.
        /// </summary>
        public DbSet<DriverRatings> DriverRatings { get; set; }
    }
}
