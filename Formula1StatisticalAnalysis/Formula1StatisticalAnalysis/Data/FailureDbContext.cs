﻿// <copyright file="FailureDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Failure databse context class.
    /// </summary>
    public class FailureDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FailureDbContext"/> class.
        /// </summary>
        /// <param name="opt">Database context option to the base class.</param>
        public FailureDbContext(DbContextOptions<FailureDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets the Failures.
        /// </summary>
        public DbSet<Failure> Failures { get; set; }
    }
}
