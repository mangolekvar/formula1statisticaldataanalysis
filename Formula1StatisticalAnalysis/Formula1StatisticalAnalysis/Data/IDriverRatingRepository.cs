﻿// <copyright file="IDriverRatingRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for drivers.
    /// </summary>
    public interface IDriverRatingRepository
    {
        /// <summary>
        /// Adds a driver rating.
        /// </summary>
        /// <param name="driverRating">Driver rating to add.</param>
        void AddDriverRating(DriverRatings driverRating);

        /// <summary>
        /// Gets all the driver ratings.
        /// </summary>
        /// <returns>Collection of driver ratings.</returns>
        IEnumerable<DriverRatings> GetAll();

        /// <summary>
        /// Gets all the driver ratings.
        /// </summary>
        /// <returns>Collection of driver ratings.</returns>
        IList<DriverRatings> GetAllToList();

        /// <summary>
        /// Deletes the driver rating table's content.
        /// </summary>
        /// <param name="driverRatings">Driver ratings to delete.</param>
        void DeleteTable(IEnumerable<DriverRatings> driverRatings);
    }
}