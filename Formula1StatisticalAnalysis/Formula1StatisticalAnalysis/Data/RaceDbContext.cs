﻿// <copyright file="RaceDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Database context for races.
    /// </summary>
    public class RaceDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RaceDbContext"/> class.
        /// </summary>
        /// <param name="opt">Context options.</param>
        public RaceDbContext(DbContextOptions<RaceDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets races.
        /// </summary>
        public DbSet<Race> Races { get; set; }
    }
}
