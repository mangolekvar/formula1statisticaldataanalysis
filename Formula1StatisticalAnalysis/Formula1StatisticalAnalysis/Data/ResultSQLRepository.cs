﻿// <copyright file="ResultSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// SQL Repository class of results.
    /// </summary>
    public class ResultSQLRepository : IResultRepository
    {
        private ResultDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Database context of results.</param>
        public ResultSQLRepository(ResultDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a result to the <see cref="context"/> databse context.
        /// </summary>
        /// <param name="result">Result to add.</param>
        public void AddResult(Result result)
        {
            this.context.Results.Add(result);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Gets all the results from the <see cref="context"/> database context.
        /// </summary>
        /// <returns>Collection of results.</returns>
        public IEnumerable<Result> GetAll()
        {
            return this.context.Results;
        }

        /// <summary>
        /// Gets all the results from the <see cref="context"/> database context.
        /// </summary>
        /// <returns>List of results.</returns>
        public IList<Result> GetAllToList()
        {
            return this.GetAll().ToList();
        }
    }
}
