﻿// <copyright file="ILapTimeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for lap times.
    /// </summary>
    public interface ILapTimeRepository
    {
        /// <summary>
        /// Adds a lap time.
        /// </summary>
        /// <param name="lapTime">Lap time to add.</param>
        void AddLapTime(LapTime lapTime);

        /// <summary>
        /// Gets all the lap times.
        /// </summary>
        /// <returns>Collection of lap times.</returns>
        IEnumerable<LapTime> GetAll();

        /// <summary>
        /// Gets all the lap times.
        /// </summary>
        /// <returns>List of lap times.</returns>
        IList<LapTime> GetAllToList();
    }
}