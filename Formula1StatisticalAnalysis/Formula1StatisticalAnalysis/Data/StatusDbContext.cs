﻿// <copyright file="StatusDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Database context class of statuses.
    /// </summary>
    public class StatusDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusDbContext"/> class.
        /// </summary>
        /// <param name="opt">Database context options.</param>
        public StatusDbContext(DbContextOptions<StatusDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets Statuses.
        /// </summary>
        public DbSet<Status> Statuses { get; set; }
    }
}
