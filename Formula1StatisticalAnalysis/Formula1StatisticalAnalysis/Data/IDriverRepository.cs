﻿// <copyright file="IDriverRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for drivers.
    /// </summary>
    public interface IDriverRepository
    {
        /// <summary>
        /// Adds a driver.
        /// </summary>
        /// <param name="driver">Driver to add.</param>
        void AddDriver(Driver driver);

        /// <summary>
        /// Gets all the drivers.
        /// </summary>
        /// <returns>Collection of drivers.</returns>
        IEnumerable<Driver> GetAll();

        /// <summary>
        /// Gets all the drivers.
        /// </summary>
        /// <returns>Collection of drivers.</returns>
        IList<Driver> GetAllToList();
    }
}