﻿// <copyright file="IRaceRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for races.
    /// </summary>
    public interface IRaceRepository
    {
        /// <summary>
        /// Adds a race.
        /// </summary>
        /// <param name="race">Race to add.</param>
        void AddRace(Race race);

        /// <summary>
        /// Gets all the races.
        /// </summary>
        /// <returns>Collection of races.</returns>
        IEnumerable<Race> GetAll();

        /// <summary>
        /// Gets all the races.
        /// </summary>
        /// <returns>List of races.</returns>
        IList<Race> GetAllToList();
    }
}