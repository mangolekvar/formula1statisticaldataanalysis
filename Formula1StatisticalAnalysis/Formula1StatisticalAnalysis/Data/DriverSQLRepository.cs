﻿// <copyright file="DriverSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// SQL Repository for drivers.
    /// </summary>
    public class DriverSQLRepository : IDriverRepository
    {
        private DriverDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="DriverSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Database context of drivers.</param>
        public DriverSQLRepository(DriverDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a driver to the <see cref="context"/> databse context.
        /// </summary>
        /// <param name="driver">Driver to add.</param>
        public void AddDriver(Driver driver)
        {
            this.context.Drivers.Add(driver);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Gets all the drivers from <see cref="context"/> database context.
        /// </summary>
        /// <returns>Collection of drivers.</returns>
        public IEnumerable<Driver> GetAll()
        {
            return this.context.Drivers;
        }

        /// <summary>
        /// Gets all the drivers from <see cref="context"/> database context.
        /// </summary>
        /// <returns>List of drivers.</returns>
        public IList<Driver> GetAllToList()
        {
            return this.context.Drivers.ToList();
        }
    }
}
