﻿// <copyright file="DriverDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Database context class for drivers.
    /// </summary>
    public class DriverDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DriverDbContext"/> class.
        /// </summary>
        /// <param name="opt">Databse context option.</param>
        public DriverDbContext(DbContextOptions<DriverDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets drivers.
        /// </summary>
        public DbSet<Driver> Drivers { get; set; }
    }
}
