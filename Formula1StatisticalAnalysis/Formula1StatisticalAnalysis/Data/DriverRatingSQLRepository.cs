﻿// <copyright file="DriverRatingSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// SQL Repository for drivers.
    /// </summary>
    public class DriverRatingSQLRepository : IDriverRatingRepository
    {
        private DriverRatingDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="DriverRatingSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Database context of drivers.</param>
        public DriverRatingSQLRepository(DriverRatingDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a driver rating to the <see cref="context"/> databse context.
        /// </summary>
        /// <param name="driverRating">Driver rating to add.</param>
        public void AddDriverRating(DriverRatings driverRating)
        {
            this.context.DriverRatings.Add(driverRating);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Deletes the driver rating table's content.
        /// </summary>
        /// <param name="driverRatings">Driver ratings to delete.</param>
        public void DeleteTable(IEnumerable<DriverRatings> driverRatings)
        {
            this.context.DriverRatings.RemoveRange(driverRatings);
        }

        /// <summary>
        /// Gets all the driver ratings from <see cref="context"/> database context.
        /// </summary>
        /// <returns>Collection of driver ratings.</returns>
        public IEnumerable<DriverRatings> GetAll()
        {
            return this.context.DriverRatings;
        }

        /// <summary>
        /// Gets all the driver ratings from <see cref="context"/> database context.
        /// </summary>
        /// <returns>List of driver ratings.</returns>
        public IList<DriverRatings> GetAllToList()
        {
            return this.GetAll().ToList();
        }
    }
}
