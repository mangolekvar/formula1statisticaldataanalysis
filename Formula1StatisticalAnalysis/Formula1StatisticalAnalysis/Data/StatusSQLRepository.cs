﻿// <copyright file="StatusSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.Data.SqlClient;

    /// <summary>
    /// SQL Repository class of statuses.
    /// </summary>
    public class StatusSQLRepository : IStatusRepository
    {
        private StatusDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Database context of statuses.</param>
        public StatusSQLRepository(StatusDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a status to the <see cref="context"/> database context.
        /// </summary>
        /// <param name="status">Status to add.</param>
        public void AddStatus(Status status)
        {
            this.context.Statuses.Add(status);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Gets all the statuses from the <see cref="context"/> database context.
        /// </summary>
        /// <returns>Collection of statuses.</returns>
        public IEnumerable<Status> GetAll()
        {
            return this.context.Statuses;
        }

        /// <summary>
        /// Gets all the statuses from the <see cref="context"/> database context.
        /// </summary>
        /// <returns>List of statuses.</returns>
        public IList<Status> GetAllToList()
        {
            return this.GetAll().ToList();
        }
    }
}
