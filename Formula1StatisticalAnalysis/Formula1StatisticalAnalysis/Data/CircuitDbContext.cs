﻿// <copyright file="CircuitDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Circuit database context class.
    /// </summary>
    public class CircuitDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitDbContext"/> class.
        /// </summary>
        /// <param name="opt">Database context option to the base class.</param>
        public CircuitDbContext(DbContextOptions<CircuitDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets the circuits.
        /// </summary>
        public DbSet<Circuit> Circuits { get; set; }
    }
}
