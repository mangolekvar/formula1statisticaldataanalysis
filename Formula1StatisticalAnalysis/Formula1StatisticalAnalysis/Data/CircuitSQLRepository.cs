﻿// <copyright file="CircuitSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// SQL repository class for circuits.
    /// </summary>
    public class CircuitSQLRepository : ICircuitRepository
    {
        private CircuitDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Database context.</param>
        public CircuitSQLRepository(CircuitDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a circuit to the database context <see cref="context"/>.
        /// </summary>
        /// <param name="circuit">Circuit to add.</param>
        public void AddCircuit(Circuit circuit)
        {
            this.context.Circuits.Add(circuit);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Gets all circuits from the databse context <see cref="circuits"/>.
        /// </summary>
        /// <returns>Circuits from the database context <see cref="context"/>.</returns>
        public IEnumerable<Circuit> GetAll()
        {
            return this.context.Circuits;
        }
    }
}
