﻿// <copyright file="ResultDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Database context class of results.
    /// </summary>
    public class ResultDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultDbContext"/> class.
        /// </summary>
        /// <param name="opt">Database context options.</param>
        public ResultDbContext(DbContextOptions<ResultDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets results.
        /// </summary>
        public DbSet<Result> Results { get; set; }
    }
}
