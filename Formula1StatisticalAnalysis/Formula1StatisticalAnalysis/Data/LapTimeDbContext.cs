﻿// <copyright file="LapTimeDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Database context of lap times.
    /// </summary>
    public class LapTimeDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LapTimeDbContext"/> class.
        /// </summary>
        /// <param name="opt">Context options.</param>
        public LapTimeDbContext(DbContextOptions<LapTimeDbContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Gets or sets lap times.
        /// </summary>
        public DbSet<LapTime> LapTimes { get; set; }

        /// <summary>
        /// Method for the multiple primary keys.
        /// </summary>
        /// <param name="modelbuilder">Model builder for the multiple primary keys.</param>
        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<LapTime>()
                .HasKey(nameof(LapTime.RaceID), nameof(LapTime.DriverID), nameof(LapTime.Lap));
        }
    }
}
