﻿// <copyright file="LapTimeSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// SQL Repository for lap times.
    /// </summary>
    public class LapTimeSQLRepository : ILapTimeRepository
    {
        private LapTimeDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="LapTimeSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Database context of lap times.</param>
        public LapTimeSQLRepository(LapTimeDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a lap time to the <see cref="context"/> database context.
        /// </summary>
        /// <param name="lapTime">Lap time to add.</param>
        public void AddLapTime(LapTime lapTime)
        {
            this.context.LapTimes.Add(lapTime);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Gets all the lap times.
        /// </summary>
        /// <returns>Collection <see cref="context"/> of lap times.</returns>
        public IEnumerable<LapTime> GetAll()
        {
            return this.context.LapTimes;
        }

        /// <summary>
        /// Gets all the lap times.
        /// </summary>
        /// <returns>List <see cref="context"/> of lap times.</returns>
        public IList<LapTime> GetAllToList()
        {
            return this.GetAll().ToList();
        }
    }
}
