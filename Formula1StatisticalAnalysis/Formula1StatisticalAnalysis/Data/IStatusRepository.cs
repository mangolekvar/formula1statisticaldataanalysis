﻿// <copyright file="IStatusRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for statuses.
    /// </summary>
    public interface IStatusRepository
    {
        /// <summary>
        /// Adds a status.
        /// </summary>
        /// <param name="status">Status to add.</param>
        void AddStatus(Status status);

        /// <summary>
        /// Gets all the statuses.
        /// </summary>
        /// <returns>Collection of statuses.</returns>
        IEnumerable<Status> GetAll();

        /// <summary>
        /// Gets all the statuses.
        /// </summary>
        /// <returns>List of statuses.</returns>
        IList<Status> GetAllToList();
    }
}