﻿// <copyright file="IResultRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for results.
    /// </summary>
    public interface IResultRepository
    {
        /// <summary>
        /// Adds a result.
        /// </summary>
        /// <param name="result">Result to add.</param>
        void AddResult(Result result);

        /// <summary>
        /// Gets all the results.
        /// </summary>
        /// <returns>Collection of results.</returns>
        IEnumerable<Result> GetAll();

        /// <summary>
        /// Gets all the results.
        /// </summary>
        /// <returns>List of results.</returns>
        IList<Result> GetAllToList();
    }
}