﻿// <copyright file="RaceSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// SQL repository class for races.
    /// </summary>
    public class RaceSQLRepository : IRaceRepository
    {
        private RaceDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="RaceSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Databse context of races.</param>
        public RaceSQLRepository(RaceDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a race to the <see cref="context"/> database context.
        /// </summary>
        /// <param name="race">Race to add.</param>
        public void AddRace(Race race)
        {
            this.context.Races.Add(race);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Gets all the races from the <see cref="context"/> databse context.
        /// </summary>
        /// <returns>Collection of races.</returns>
        public IEnumerable<Race> GetAll()
        {
            return this.context.Races;
        }

        /// <summary>
        /// Gets all the races from the <see cref="context"/> databse context.
        /// </summary>
        /// <returns>List of races.</returns>
        public IList<Race> GetAllToList()
        {
            return this.GetAll().ToList();
        }
    }
}
