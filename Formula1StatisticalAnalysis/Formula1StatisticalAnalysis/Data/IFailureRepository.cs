﻿// <copyright file="IFailureRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for failures.
    /// </summary>
    public interface IFailureRepository
    {
        /// <summary>
        /// Adds a driver rating.
        /// </summary>
        /// <param name="failure">Failure to add.</param>
        void AddFailure(Failure failure);

        /// <summary>
        /// Gets all the failures.
        /// </summary>
        /// <returns>Collection of failures.</returns>
        IEnumerable<Failure> GetAll();

        /// <summary>
        /// Gets all the failures.
        /// </summary>
        /// <returns>Collection of failures.</returns>
        IList<Failure> GetAllToList();

        /// <summary>
        /// Deletes the failure table's content.
        /// </summary>
        /// <param name="failures">Failures to delete.</param>
        void DeleteTable(IEnumerable<Failure> failures);
    }
}