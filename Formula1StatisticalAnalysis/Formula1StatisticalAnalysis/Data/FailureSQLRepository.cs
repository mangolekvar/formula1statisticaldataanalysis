﻿// <copyright file="FailureSQLRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// SQL Repository for drivers.
    /// </summary>
    public class FailureSQLRepository : IFailureRepository
    {
        private FailureDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="FailureSQLRepository"/> class.
        /// </summary>
        /// <param name="context">Database context of failures.</param>
        public FailureSQLRepository(FailureDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds a failure to the <see cref="context"/> databse context.
        /// </summary>
        /// <param name="failure">Failure to add.</param>
        public void AddFailure(Failure failure)
        {
            this.context.Failures.Add(failure);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Deletes the failure table's content.
        /// </summary>
        /// <param name="failures">Failure to delete.</param>
        public void DeleteTable(IEnumerable<Failure> failures)
        {
            this.context.Failures.RemoveRange(failures);
        }

        /// <summary>
        /// Gets all the failures from <see cref="context"/> database context.
        /// </summary>
        /// <returns>Collection of failures.</returns>
        public IEnumerable<Failure> GetAll()
        {
            return this.context.Failures;
        }

        /// <summary>
        /// Gets all the failures from <see cref="context"/> database context.
        /// </summary>
        /// <returns>List of failures.</returns>
        public IList<Failure> GetAllToList()
        {
            return this.GetAll().ToList();
        }
    }
}
