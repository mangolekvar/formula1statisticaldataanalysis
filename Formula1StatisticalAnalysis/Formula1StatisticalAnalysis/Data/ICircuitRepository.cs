﻿// <copyright file="ICircuitRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Data
{
    using System.Collections.Generic;
    using Formula1StatisticalAnalysis.Models;

    /// <summary>
    /// Repository interface for circuits.
    /// </summary>
    public interface ICircuitRepository
    {
        /// <summary>
        /// Adds a circuit.
        /// </summary>
        /// <param name="circuit">Circuit to add.</param>
        void AddCircuit(Circuit circuit);

        /// <summary>
        /// Gets all circuits.
        /// </summary>
        /// <returns>Collection of circuits.</returns>
        IEnumerable<Circuit> GetAll();
    }
}