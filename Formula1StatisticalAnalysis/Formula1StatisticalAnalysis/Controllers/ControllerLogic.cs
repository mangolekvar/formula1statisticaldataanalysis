﻿// <copyright file="ControllerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Data;
    using Formula1StatisticalAnalysis.Models;
    using Formula1StatisticalAnalysis.ViewModels;
    using MathNet;
    using MathNet.Numerics;

    /// <summary>
    /// Logic class for HomeController.
    /// </summary>
    public class ControllerLogic
    {
        #region Variables
        private IStatusRepository statusRepo;
        private IEnumerable<Status> statuses;
        private IEnumerable<string> newStatuses;
        private ICircuitRepository circuitRepo;
        private IEnumerable<Circuit> circuits;
        private IEnumerable<string> newCircuits;
        private IRaceRepository raceRepo;
        private IEnumerable<Race> races;
        private IEnumerable<string> newRaces;
        private IDriverRepository driverRepo;
        private IEnumerable<Driver> drivers;
        private IEnumerable<string> newDrivers;
        private IResultRepository resultRepo;
        private IEnumerable<Result> results;
        private IEnumerable<string> newResults;
        private ILapTimeRepository lapTimeRepo;
        private IEnumerable<LapTime> lapTimes;
        private IEnumerable<string> newLapTimes;
        private IDriverRatingRepository driverRatingRepo;
        private IEnumerable<DriverRatings> driverRatings;
        private bool newDataForDriverRatings;
        private IEnumerable<Championship> championship;
        private IList<int> years;
        private ChampionshipVM championshipVM;
        private IList<int> rounds;
        private int championshipSelectedYear;
        private IList<int> championshipSelectedRounds;
        private IEnumerable<GrandPrix> grandprixes;
        private GrandPrixVM grandPrixVM;
        private int grandPrixSelectedYear;
        private int grandPrixSelectedRound;
        private IList<int> grandPrixSelectedLaps;
        private IFailureRepository failureRepo;
        private IEnumerable<Failure> failures;
        private string convertedPoints;
        private string convertedLapTime;
        private IList<LapTimePrediction> linearLapTimePrediction;
        private IList<LapTimePrediction> polynomialLapTimePrediction;
        private int selectedCircuitID;
        private LapTimePredictionVM lapTimePredictionVM;
        private IEnumerable<LapTime> sortedDataForLapTimePrediction;
        private FailuresVM failuresVM;
        private int failureSelectedYear;
        private IList<int> roundsByYears;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerLogic"/> class.
        /// </summary>
        /// <param name="statusRepo">Status repository.</param>
        /// <param name="circuitRepo">Circuit repository.</param>
        /// <param name="raceRepo">Race repository.</param>
        /// <param name="driverRepo">Driver repository.</param>
        /// <param name="resultRepo">Result repository.</param>
        /// <param name="lapTimeRepo">Lap time repository.</param>
        /// <param name="driverRatingRepo">Driver rating repository.</param>
        /// <param name="failureRepo">Failure repository.</param>
        public ControllerLogic(IStatusRepository statusRepo, ICircuitRepository circuitRepo, IRaceRepository raceRepo, IDriverRepository driverRepo, IResultRepository resultRepo, ILapTimeRepository lapTimeRepo, IDriverRatingRepository driverRatingRepo, IFailureRepository failureRepo)
        {
            this.newDataForDriverRatings = true;

            this.RefreshDatabases(statusRepo, circuitRepo, raceRepo, driverRepo, resultRepo, lapTimeRepo, driverRatingRepo, failureRepo);
            this.driverRatings = this.driverRatingRepo.GetAll().OrderByDescending(x => x.DriverRating);
            this.failures = this.failureRepo.GetAll();

            this.roundsByYears = new List<int>();
            this.years = this.GetYears();
            this.rounds = this.GetRounds();
            this.championshipVM = new ChampionshipVM();
            this.championshipVM.RoundsByYears = this.roundsByYears;
            this.championshipVM.Years = this.years;
            this.grandPrixVM = new GrandPrixVM();
            this.grandPrixVM.Years = this.years;
            this.grandPrixVM.Rounds = this.rounds;
            this.grandPrixVM.GrandPrixInfosByRaces = this.GetGrandPrixInfos();
            this.championshipSelectedRounds = new List<int>();
            this.championshipSelectedRounds.Add(0);
            this.championshipSelectedYear = 2021;
            this.grandPrixSelectedYear = 2021;
            this.grandPrixSelectedRound = 1;
            this.grandPrixSelectedLaps = new List<int>();
            this.grandPrixSelectedLaps.Add(0);
            this.linearLapTimePrediction = new List<LapTimePrediction>();
            this.polynomialLapTimePrediction = new List<LapTimePrediction>();
            this.selectedCircuitID = 1;
            this.lapTimePredictionVM = new LapTimePredictionVM();
            this.lapTimePredictionVM.Circuits = this.circuitRepo.GetAll().ToList();
            this.failuresVM = new FailuresVM();
            this.failuresVM.Years = this.years;
            this.failureSelectedYear = 2021;
            this.failuresVM.SelectedYear = this.failureSelectedYear;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerLogic"/> class.
        /// </summary>
        /// <param name="statusRepo">Status repository.</param>
        /// <param name="circuitRepo">Circuit repository.</param>
        /// <param name="raceRepo">Race repository.</param>
        /// <param name="driverRepo">Driver repository.</param>
        /// <param name="resultRepo">Result repository.</param>
        /// <param name="lapTimeRepo">Lap time repository.</param>
        /// <param name="driverRatingRepo">Driver rating repository.</param>
        /// <param name="failureRepo">Failure repository.</param>
        /// <param name="nothing">Used for unit test.</param>
        public ControllerLogic(IStatusRepository statusRepo, ICircuitRepository circuitRepo, IRaceRepository raceRepo, IDriverRepository driverRepo, IResultRepository resultRepo, ILapTimeRepository lapTimeRepo, IDriverRatingRepository driverRatingRepo, IFailureRepository failureRepo, int nothing)
        {
            this.newDataForDriverRatings = true;

            // this.RefreshDatabases(statusRepo, circuitRepo, raceRepo, driverRepo, resultRepo, lapTimeRepo, driverRatingRepo, failureRepo);
            this.circuitRepo = circuitRepo;
            this.raceRepo = raceRepo;
            this.statusRepo = statusRepo;
            this.driverRepo = driverRepo;
            this.lapTimeRepo = lapTimeRepo;
            this.resultRepo = resultRepo;
            this.driverRatingRepo = driverRatingRepo;
            this.failureRepo = failureRepo;

            // this.driverRatings = this.driverRatingRepo.GetAll().OrderByDescending(x => x.DriverRating);
            this.failures = this.failureRepo.GetAll();

            this.roundsByYears = new List<int>();
            this.years = this.GetYears();
            this.rounds = this.GetRounds();
            this.championshipVM = new ChampionshipVM();
            this.championshipVM.RoundsByYears = this.roundsByYears;
            this.championshipVM.Years = this.years;
            this.grandPrixVM = new GrandPrixVM();
            this.grandPrixVM.Years = this.years;
            this.grandPrixVM.Rounds = this.rounds;
            this.grandPrixVM.GrandPrixInfosByRaces = this.GetGrandPrixInfos();
            this.championshipSelectedRounds = new List<int>();
            this.championshipSelectedRounds.Add(0);
            this.championshipSelectedYear = 2021;
            this.grandPrixSelectedYear = 2021;
            this.grandPrixSelectedRound = 1;
            this.grandPrixSelectedLaps = new List<int>();
            this.grandPrixSelectedLaps.Add(0);
            this.linearLapTimePrediction = new List<LapTimePrediction>();
            this.polynomialLapTimePrediction = new List<LapTimePrediction>();
            this.selectedCircuitID = 1;
            this.lapTimePredictionVM = new LapTimePredictionVM();
            this.lapTimePredictionVM.Circuits = this.circuitRepo.GetAll().ToList();
            this.failuresVM = new FailuresVM();
            this.failuresVM.Years = this.years;
            this.failureSelectedYear = 2021;
            this.failuresVM.SelectedYear = this.failureSelectedYear;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets Championship view model.
        /// </summary>
        public ChampionshipVM ChampionshipViewModel
        {
            get { return this.championshipVM; }
            set { this.championshipVM = value; }
        }

        /// <summary>
        /// Gets or sets Championship view model.
        /// </summary>
        public GrandPrixVM GrandPrixViewModel
        {
            get { return this.grandPrixVM; }
            set { this.grandPrixVM = value; }
        }

        /// <summary>
        /// Gets or sets driver ratings.
        /// </summary>
        public IEnumerable<DriverRatings> DriverRatings
        {
            get { return this.driverRatings; }
            set { this.driverRatings = value; }
        }

        /// <summary>
        /// Gets or sets driver ratings.
        /// </summary>
        public IEnumerable<Failure> Failures
        {
            get { return this.failures; }
            set { this.failures = value; }
        }

        /// <summary>
        /// Gets or sets Lap time prediction view model.
        /// </summary>
        public LapTimePredictionVM LapTimePredictionViewModel
        {
            get { return this.lapTimePredictionVM; }
            set { this.lapTimePredictionVM = value; }
        }

        /// <summary>
        /// Gets or sets Failure view model.
        /// </summary>
        public FailuresVM FailureViewModel
        {
            get { return this.failuresVM; }
            set { this.failuresVM = value; }
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Gets lap time predictions.
        /// </summary>
        public void GetLapTimePrediction()
        {
            this.CalculateLapTimePrediction(this.selectedCircuitID);
            this.lapTimePredictionVM.LapTimePredictions = this.polynomialLapTimePrediction;
            this.lapTimePredictionVM.SelectedCircuitID = this.selectedCircuitID;
        }

        /// <summary>
        /// Gets lap time predictions.
        /// </summary>
        /// <param name="circuitID">Circuit to select.</param>
        public void GetLapTimePrediction(int circuitID)
        {
            this.selectedCircuitID = circuitID;
            this.CalculateLapTimePrediction(this.selectedCircuitID);
            this.lapTimePredictionVM.LapTimePredictions = this.polynomialLapTimePrediction;
            this.lapTimePredictionVM.SelectedCircuitID = this.selectedCircuitID;
        }

        /// <summary>
        /// Calculates lap time prediction for a circuit.
        /// </summary>
        /// <param name="circuitID">ID of the circuit to calculate the data.</param>
        public void CalculateLapTimePrediction(int circuitID)
        {
            var bestLap = from lapTime in this.lapTimeRepo.GetAllToList()
                          group lapTime by lapTime.RaceID into grp
                          select new
                          {
                              RaceID = grp.Key,
                              BestLap = grp.Min(x => x.Milliseconds),
                          };

            IEnumerable<int> years = (from lapTime in this.lapTimeRepo.GetAllToList()
                                      join race in this.raceRepo.GetAllToList()
                                      on lapTime.RaceID equals race.RaceID
                                      where race.CircuitID == circuitID
                                      group race by race.Year into grp
                                      orderby grp.Key descending
                                      select grp.Key).Take(1);
            int year = 0;
            foreach (var item in years)
            {
                year = item;
            }

            IEnumerable<int> raceIDs = (from lapTime in this.lapTimeRepo.GetAllToList()
                                        join race in this.raceRepo.GetAllToList()
                                        on lapTime.RaceID equals race.RaceID
                                        where race.CircuitID == circuitID && race.Year == year
                                        group race by race.RaceID into grp
                                        orderby grp.Key descending
                                        select grp.Key).Take(1);

            int raceID = 0;
            foreach (var item in raceIDs)
            {
                raceID = item;
            }

            var bestLaps = from lapTime in this.lapTimeRepo.GetAllToList()
                           where lapTime.RaceID == raceID && lapTime.RaceID != 1046
                           group lapTime by new { lapTime.RaceID, lapTime.Lap } into grp
                           select new
                           {
                               RaceIDLap = grp.Key.RaceID + ":" + grp.Key.Lap,
                               BestLap = grp.Min(x => x.Milliseconds),
                           };

            this.sortedDataForLapTimePrediction = from lapTime in this.lapTimeRepo.GetAllToList()
                                                  join best1 in bestLaps
                                                  on lapTime.RaceID + ":" + lapTime.Lap equals best1.RaceIDLap
                                                  join best2 in bestLap
                                                  on lapTime.RaceID equals best2.RaceID
                                                  where lapTime.RaceID == raceID && lapTime.Milliseconds < best2.BestLap * 1.5
                                                  select new LapTime()
                                                  {
                                                      DriverID = lapTime.DriverID,
                                                      Lap = lapTime.Lap,
                                                      Milliseconds = lapTime.Milliseconds,
                                                      RaceID = lapTime.RaceID,
                                                  };

            this.lapTimePredictionVM.LapTimes = this.sortedDataForLapTimePrediction;

            IList<double> laps = new List<double>();
            IList<double> milliseconds = new List<double>();
            foreach (var item in this.sortedDataForLapTimePrediction)
            {
                if (item.Lap != 1)
                {
                    laps.Add(item.Lap);
                    milliseconds.Add(item.Milliseconds);
                }
            }

            // Tuple<double, double> result = Fit.Line(laps.ToArray(), milliseconds.ToArray());

            // double intercept = result.Item1;
            // double slope = result.Item2;

            // if (slope > 0)
            // {
            //    for (int i = 0; i < laps.Distinct().Count(); i++)
            //    {
            //        this.linearLapTimePrediction.Add(
            //            new LapTimePrediction()
            //            {
            //                Lap = i + 1,
            //                Milliseconds = intercept + (i * slope),
            //                Time = (int)(((intercept + (i * slope)) / 1000) / 60)
            //                        + ":" + (int)(((intercept + (i * slope)) / 1000) % 60)
            //                        + "." + ((intercept + (i * slope)) % 1000).Round(0),
            //            });
            //    }
            // }
            // else
            // {
            //    for (int i = 0; i < laps.Distinct().Count(); i++)
            //    {
            //        this.linearLapTimePrediction.Add(
            //            new LapTimePrediction()
            //            {
            //                Lap = i + 1,
            //                Milliseconds = intercept + (i * slope),
            //                Time = (int)(((intercept - (i * Math.Abs(slope))) / 1000) / 60) + ":" + (int)(((intercept - (i * Math.Abs(slope))) / 1000) % 60) + "." + ((intercept - (i * Math.Abs(slope))) % 1000).Round(0),
            //            });
            //    }
            // }
            double[] p = Fit.Polynomial(laps.ToArray(), milliseconds.ToArray(), 2);

            for (int j = 0; j < laps.Max(); j++)
            {
                double time = 0;

                for (int i = 0; i < p.Count(); i++)
                {
                    time = time + (p[i] * Math.Pow(j + 1, i));
                }

                this.polynomialLapTimePrediction.Add(
                    new LapTimePrediction()
                    {
                        Lap = j + 1,
                        Milliseconds = time,
                        Time = (int)((time / 1000) / 60) + ":" + (int)((time / 1000) % 60) + "." + (time % 1000).Round(0),
                    });
            }
        }

        /// <summary>
        /// Refreshing the database: adding new lines from the source datasets.
        /// </summary>
        /// <param name="statusRepo">Status repository.</param>
        /// <param name="circuitRepo">Circuit repository.</param>
        /// <param name="raceRepo">Race repository.</param>
        /// <param name="driverRepo">Driver repository.</param>
        /// <param name="resultRepo">Result repository.</param>
        /// <param name="lapTimeRepo">Lap time repository.</param>
        /// <param name="driverRatingRepo">Driver rating repository.</param>
        /// <param name="failureRepo">Failure repository.</param>
        public void RefreshDatabases(IStatusRepository statusRepo, ICircuitRepository circuitRepo, IRaceRepository raceRepo, IDriverRepository driverRepo, IResultRepository resultRepo, ILapTimeRepository lapTimeRepo, IDriverRatingRepository driverRatingRepo, IFailureRepository failureRepo)
        {
            this.statusRepo = statusRepo;
            this.statuses = this.statusRepo.GetAll();
            this.newStatuses = System.IO.File.ReadLines("Datasets/status.zip").Skip(this.statuses.Count() + 1);
            foreach (string line in this.newStatuses)
            {
                this.statusRepo.AddStatus(
                     new Status(
                         int.Parse(line.Split(',')[0]),
                         line.Split('"')[1]));
            }

            this.circuitRepo = circuitRepo;
            this.circuits = this.circuitRepo.GetAll();
            this.newCircuits = System.IO.File.ReadLines("Datasets/circuits.zip").Skip(this.circuits.Count() + 1);
            foreach (string line in this.newCircuits)
            {
                this.circuitRepo.AddCircuit(
                     new Circuit(
                         int.Parse(line.Split(',')[0]),
                         line.Split('"')[3],
                         line.Split('"')[5],
                         line.Split('"')[7]));
            }

            this.raceRepo = raceRepo;
            this.races = this.raceRepo.GetAll();
            this.newRaces = System.IO.File.ReadLines("Datasets/races.zip").Skip(this.races.Count() + 1);
            foreach (string line in this.newRaces)
            {
                this.raceRepo.AddRace(
                    new Race(
                        int.Parse(line.Split(',')[0]),
                        int.Parse(line.Split(',')[1]),
                        int.Parse(line.Split(',')[2]),
                        int.Parse(line.Split(',')[3]),
                        line.Split('"')[1]));
            }

            this.driverRepo = driverRepo;
            this.drivers = this.driverRepo.GetAll();
            this.newDrivers = System.IO.File.ReadLines("Datasets/drivers.zip").Skip(this.drivers.Count() + 1);
            if (this.newDrivers.Count() == 0)
            {
                this.newDataForDriverRatings = false;
            }
            else
            {
                foreach (string line in this.newDrivers)
                {
                    if (line.Split('"')[2].Contains("\\N,\\N"))
                    {
                        this.driverRepo.AddDriver(
                            new Driver(
                                int.Parse(line.Split(',')[0]),
                                line.Split('"')[3] + " " + line.Split('"')[5]));
                    }
                    else
                    {
                        this.driverRepo.AddDriver(
                            new Driver(
                                int.Parse(line.Split(',')[0]),
                                line.Split('"')[5] + " " + line.Split('"')[7]));
                    }
                }
            }

            this.resultRepo = resultRepo;
            this.results = this.resultRepo.GetAll();
            this.newResults = System.IO.File.ReadLines("Datasets/results.zip").Skip(this.results.Count() + 1);
            if (this.newResults.Count() == 0)
            {
                this.newDataForDriverRatings = false;
            }
            else
            {
                foreach (string line in this.newResults)
                {
                    if (line.Split(',')[9].Contains('.'))
                    {
                        this.convertedPoints = line.Split(',')[9].Split('.')[0] + ',' + line.Split(',')[9].Split('.')[1];
                    }
                    else
                    {
                        this.convertedPoints = line.Split(',')[9];
                    }

                    if (line.Split(',')[12].Contains("N"))
                    {
                        this.convertedLapTime = "0";
                    }
                    else
                    {
                        this.convertedLapTime = line.Split(',')[12];
                    }

                    this.resultRepo.AddResult(
                    new Result(
                        int.Parse(line.Split(',')[0]),
                        int.Parse(line.Split(',')[1]),
                        int.Parse(line.Split(',')[2]),
                        int.Parse(line.Split(',')[3]),
                        int.Parse(line.Split(',')[8]),
                        Convert.ToDouble(this.convertedPoints),
                        int.Parse(this.convertedLapTime),
                        int.Parse(line.Split(',')[17])));
                }
            }

            this.lapTimeRepo = lapTimeRepo;
            this.lapTimes = this.lapTimeRepo.GetAll();
            this.newLapTimes = System.IO.File.ReadLines("Datasets/lap_times.zip").Skip(this.lapTimes.Count() + 1);
            foreach (string line in this.newLapTimes)
            {
                this.lapTimeRepo.AddLapTime(
                new LapTime(
                    int.Parse(line.Split(',')[0]),
                    int.Parse(line.Split(',')[1]),
                    int.Parse(line.Split(',')[2]),
                    int.Parse(line.Split(',')[5])));
            }

            this.driverRatingRepo = driverRatingRepo;
            this.driverRatings = this.driverRatingRepo.GetAll();
            this.failureRepo = failureRepo;
            this.failures = this.failureRepo.GetAll();
        }

        /// <summary>
        /// Calculates new data for driver ratings if there's new data for it in the database.
        /// </summary>
        public void CalculateDriverRatings()
        {
            if (this.newDataForDriverRatings)
            {
                this.CalculateDriverRatingsLogic();
                this.driverRatings = this.driverRatingRepo.GetAll().OrderByDescending(x => x.DriverRating);
            }
        }

        /// <summary>
        /// Gets championship data by the selected year.
        /// </summary>
        public void GetChampionship()
        {
            this.GetChampionshipLogic(this.championshipSelectedYear, new int[] { 0 });
            this.championshipVM.Championships = this.championship;
            this.championshipVM.SelectedYear = this.championshipSelectedYear;
            this.championshipVM.SelectedRounds = this.championshipSelectedRounds;
        }

        /// <summary>
        /// Gets championship data by the selected year. and rounds.
        /// </summary>
        /// <param name="year">Year to select.</param>
        /// <param name="minRound">Minimum value of round.</param>
        /// <param name="maxRound">Maximum value of round.</param>
        public void GetChampionship(int year, int minRound, int maxRound)
        {
            this.championshipSelectedYear = year;
            this.championshipSelectedRounds.Clear();
            for (int i = minRound; i < maxRound + 1; i++)
            {
                this.championshipSelectedRounds.Add(i);
            }

            this.GetChampionshipLogic(this.championshipSelectedYear, this.championshipSelectedRounds.ToArray());
            this.championshipVM.Championships = this.championship;
            this.championshipVM.SelectedYear = this.championshipSelectedYear;
            this.championshipVM.SelectedRounds = this.championshipSelectedRounds;
        }

        /// <summary>
        /// Gets the grand prixes.
        /// </summary>
        public void GetGrandPrixes()
        {
            this.GetGrandPrixesLogic(this.grandPrixSelectedYear, this.grandPrixSelectedRound, new int[] { 0 });

            this.grandPrixVM.GrandPrixes = this.grandprixes;
            this.grandPrixVM.SelectedYear = this.grandPrixSelectedYear;
            this.grandPrixVM.SelectedRound = this.grandPrixSelectedRound;
            this.grandPrixVM.SelectedLaps = this.grandPrixSelectedLaps;
        }

        /// <summary>
        /// Gets the grand prixes.
        /// </summary>
        /// <param name="year">Year to select.</param>
        /// <param name="round">Round to select.</param>
        /// <param name="minLaps">Minimum Laps to select.</param>
        /// <param name="maxLaps">Maximum Laps to select.</param>
        public void GetGrandPrixes(int year, int round, int minLaps, int maxLaps)
        {
            this.grandPrixSelectedYear = year;
            this.grandPrixSelectedRound = round;
            this.grandPrixSelectedLaps.Clear();
            for (int i = minLaps; i < maxLaps + 1; i++)
            {
                this.grandPrixSelectedLaps.Add(i);
            }

            this.GetGrandPrixesLogic(this.grandPrixSelectedYear, this.grandPrixSelectedRound, this.grandPrixSelectedLaps.ToArray());
            this.grandPrixVM.GrandPrixes = this.grandprixes;
            this.grandPrixVM.SelectedYear = this.grandPrixSelectedYear;
            this.grandPrixVM.SelectedRound = this.grandPrixSelectedRound;
            this.grandPrixVM.SelectedLaps = this.grandPrixSelectedLaps;
        }

        /// <summary>
        /// Gets the failures in descending order of the quantity.
        /// </summary>
        public void CalculateFailures()
        {
            this.CalculateFailures(this.failureSelectedYear);
        }

        /// <summary>
        /// Gets the failures in descending order of the quantity.
        /// </summary>
        /// <param name="year">Year to select.</param>
        public void CalculateFailures(int year)
        {
            this.failureSelectedYear = year;
            this.failuresVM.SelectedYear = this.failureSelectedYear;
            this.CalculateFailuresLogic(year);
            this.failuresVM.Failures = this.failureRepo.GetAll().OrderByDescending(x => x.Quantity);
        }

        /// <summary>
        /// Gtes all the availabel laps.
        /// </summary>
        /// <returns>List of laps.</returns>
        public IList<int> GetLaps()
        {
            return this.lapTimeRepo.GetAllToList()
                .GroupBy(x => x.Lap)
                .OrderBy(x => x.Key)
                .Select(x => x.Key)
                .ToList();
        }

        /// <summary>
        /// Gets all the available rounds.
        /// </summary>
        /// <returns>List of rounds.</returns>
        public IList<int> GetRounds()
        {
            return this.raceRepo.GetAllToList()
                .GroupBy(x => x.Round)
                .OrderBy(x => x.Key)
                .Select(x => x.Key)
                .ToList();
        }

        /// <summary>
        /// Gets years from races table.
        /// </summary>
        /// <returns>All the available years.</returns>
        public IList<int> GetYears()
        {
            this.roundsByYears = this.raceRepo.GetAllToList()
                .GroupBy(x => x.Year)
                .OrderByDescending(x => x.Key)
                .Select(x => x.Count())
                .ToList();

            return this.raceRepo.GetAllToList()
                .GroupBy(x => x.Year)
                .OrderByDescending(x => x.Key)
                .Select(x => x.Key)
                .ToList();
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Gets Grand prix infos.
        /// </summary>
        /// <returns>List of Grand prix infos.</returns>
        private List<GrandPrixInfo> GetGrandPrixInfos()
        {
            return (from races in this.raceRepo.GetAllToList()
                    join laps in this.lapTimeRepo.GetAllToList()
                    on races.RaceID equals laps.RaceID
                    orderby races.Year descending, races.Round
                    select new
                    {
                        Year = races.Year,
                        Round = races.Round,
                        Name = races.Name,
                        RaceID = laps.RaceID,
                        Lap = laps.Lap,
                    }
                    into joinedData
                    group joinedData by joinedData.RaceID into grp
                    select new GrandPrixInfo()
                    {
                        Year = grp.Max(x => x.Year),
                        Round = grp.Max(x => x.Round),
                        Name = grp.Max(x => x.Name),
                        NOLaps = grp.Max(x => x.Lap),
                    }).ToList();
        }

        /// <summary>
        /// Gets the failures.
        /// </summary>
        /// <param name="year">Year to select.</param>
        private void CalculateFailuresLogic(int year)
        {
            this.failureRepo.DeleteTable(this.failures);
            var allowedStatus = new[] { 1, 2, 3, 4, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 45, 50, 53, 54, 55, 58, 62, 77, 81, 88, 90, 97, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 122, 123, 124, 125, 127, 128, 133, 134 };

            IEnumerable<Failure> finaldata = from result in this.resultRepo.GetAllToList()
                                             join status in this.statusRepo.GetAllToList()
                                             on result.StatusID equals status.StatusID
                                             join race in this.raceRepo.GetAllToList()
                                             on result.RaceID equals race.RaceID
                                             where !allowedStatus.Contains(status.StatusID) && race.Year == year
                                             select new
                                             {
                                                 ResultID = result.ResultID,
                                                 Status = status.RealStatus,
                                                 RaceID = race.RaceID,
                                             }
                                             into data
                                             group data by data.Status into grp
                                             select new
                                             {
                                                 Status = grp.Key,
                                                 Quantity = grp.Count(),
                                             }
                                             into finalData
                                             orderby finalData.Quantity descending
                                             select new Failure()
                                             {
                                                 Status = finalData.Status,
                                                 Quantity = finalData.Quantity,
                                             };

            foreach (Failure item in finaldata)
            {
                this.failureRepo.AddFailure(item);
            }
        }

        /// <summary>
        /// Gets the data for GrandPrix view.
        /// </summary>
        /// <param name="year">Year to select.</param>
        /// <param name="round">Round to select.</param>
        /// <param name="laps">Laps to select.</param>
        private void GetGrandPrixesLogic(int year, int round, int[] laps)
        {
            if (laps.Contains(0))
            {
                var bestLaps = from lapTime2 in this.lapTimeRepo.GetAllToList()
                               join race in this.raceRepo.GetAllToList()
                               on lapTime2.RaceID equals race.RaceID
                               where race.Year == year && race.Round == round
                               select new
                               {
                                   RaceID = lapTime2.RaceID,
                                   Lap = lapTime2.Lap,
                                   Milliseconds = lapTime2.Milliseconds,
                               }
                           into lapTime
                               group lapTime by new { lapTime.RaceID, lapTime.Lap } into grp
                               select new
                               {
                                   RaceIDLap = grp.Key.RaceID + "," + grp.Key.Lap,
                                   BestLap = grp.Min(x => x.Milliseconds),
                               };

                var lapTimeWithGap = from lapTime in this.lapTimeRepo.GetAllToList()
                                     join race in this.raceRepo.GetAllToList()
                                     on lapTime.RaceID equals race.RaceID
                                     join bestLap in bestLaps
                                     on lapTime.RaceID + "," + lapTime.Lap equals bestLap.RaceIDLap
                                     where race.Year == year && race.Round == round
                                     select new
                                     {
                                         RaceID = lapTime.RaceID,
                                         DriverID = lapTime.DriverID,
                                         Lap = lapTime.Lap,
                                         Gap = lapTime.Milliseconds - bestLap.BestLap,
                                         Milliseconds = lapTime.Milliseconds,
                                     };

                var lastLap = from lapTime2 in this.lapTimeRepo.GetAllToList()
                              join race in this.raceRepo.GetAllToList()
                              on lapTime2.RaceID equals race.RaceID
                              where race.Year == year && race.Round == round
                              select new
                              {
                                  RaceID = lapTime2.RaceID,
                                  Lap = lapTime2.Lap,
                              }
                              into lapTime
                              group lapTime by lapTime.RaceID into grp
                              select new
                              {
                                  RaceID = grp.Key,
                                  LastLap = grp.Max(x => x.Lap),
                              };

                var bestTime = from lapTime2 in this.lapTimeRepo.GetAllToList()
                               join race in this.raceRepo.GetAllToList()
                               on lapTime2.RaceID equals race.RaceID
                               where race.Year == year && race.Round == round
                               select new
                               {
                                   RaceID = lapTime2.RaceID,
                                   DriverID = lapTime2.DriverID,
                                   Milliseconds = lapTime2.Milliseconds,
                                   Lap = lapTime2.Lap,
                               }
                               into lapTime
                               group lapTime by new { lapTime.RaceID, lapTime.DriverID } into grp
                               select new
                               {
                                   RaceID = grp.Key.RaceID,
                                   DriverID = grp.Key.DriverID,
                                   Time = grp.Sum(x => x.Milliseconds),
                                   Laps = grp.Max(x => x.Lap),
                               }
                               into grouping
                               join lastLaps in lastLap
                               on grouping.RaceID equals lastLaps.RaceID
                               where grouping.Laps == lastLaps.LastLap
                               select new
                               {
                                   RaceID = grouping.RaceID,
                                   DriverID = grouping.DriverID,
                                   Time = grouping.Time,
                               }
                               into selection
                               group selection by selection.RaceID into grp
                               select new
                               {
                                   RaceID = grp.Key,
                                   BestTime = grp.Min(x => x.Time),
                               };

                var winnerAndBestTime = from lapTime2 in this.lapTimeRepo.GetAllToList()
                                        join race in this.raceRepo.GetAllToList()
                                        on lapTime2.RaceID equals race.RaceID
                                        where race.Year == year && race.Round == round
                                        select new
                                        {
                                            RaceID = lapTime2.RaceID,
                                            DriverID = lapTime2.DriverID,
                                            Milliseconds = lapTime2.Milliseconds,
                                        }
                                        into lapTime
                                        group lapTime by new { lapTime.RaceID, lapTime.DriverID } into grp
                                        select new
                                        {
                                            RaceID = grp.Key.RaceID,
                                            DriverID = grp.Key.DriverID,
                                            Time = grp.Sum(x => x.Milliseconds),
                                        }
                                        into selection
                                        join bestTimes in bestTime
                                        on selection.RaceID equals bestTimes.RaceID
                                        where selection.Time == bestTimes.BestTime
                                        select new
                                        {
                                            RaceID = selection.RaceID,
                                            DriverID = selection.DriverID,
                                            BestTime = bestTimes.BestTime,
                                        };

                var winnerGap = from lapTime in lapTimeWithGap
                                join driver in this.driverRepo.GetAllToList()
                                on lapTime.DriverID equals driver.DriverID
                                join race in this.raceRepo.GetAllToList()
                                on lapTime.RaceID equals race.RaceID
                                where race.Year == year && race.Round == round
                                select new
                                {
                                    RaceID = lapTime.RaceID,
                                    DriverID = lapTime.DriverID,
                                    Gap = lapTime.Gap,
                                }
                                into data
                                group data by new { data.DriverID, data.RaceID } into grp
                                select new
                                {
                                    DriverID = grp.Key.DriverID,
                                    RaceID = grp.Key.RaceID,
                                    Gap = grp.Sum(x => x.Gap),
                                }
                                into final
                                join winner in winnerAndBestTime
                                on final.RaceID equals winner.RaceID
                                where final.DriverID == winner.DriverID
                                select new
                                {
                                    RaceID = winner.RaceID,
                                    DriverID = winner.DriverID,
                                    BestTime = winner.BestTime,
                                    BestGap = final.Gap,
                                }
                                into reallyFinal
                                join lap in lastLap
                                on reallyFinal.RaceID equals lap.RaceID
                                select new
                                {
                                    RaceID = reallyFinal.RaceID,
                                    DriverID = reallyFinal.DriverID,
                                    BestTime = reallyFinal.BestTime,
                                    BestGap = reallyFinal.BestGap,
                                    LastLap = lap.LastLap,
                                };

                this.grandprixes = from lapTime in lapTimeWithGap
                                   join driver in this.driverRepo.GetAllToList()
                                   on lapTime.DriverID equals driver.DriverID
                                   join race in this.raceRepo.GetAllToList()
                                   on lapTime.RaceID equals race.RaceID
                                   where race.Year == year && race.Round == round
                                   select new
                                   {
                                       RaceID = lapTime.RaceID,
                                       DriverID = lapTime.DriverID,
                                       DriverName = driver.Name,
                                       Gap = lapTime.Gap,
                                       Lap = lapTime.Lap,
                                       Milliseconds = lapTime.Milliseconds,
                                   }
                                   into data
                                   group data by new { data.RaceID, data.DriverID, data.DriverName } into grp
                                   select new
                                   {
                                       RaceID = grp.Key.RaceID,
                                       DriverID = grp.Key.DriverID,
                                       DriverName = grp.Key.DriverName,
                                       Gap = grp.Sum(x => x.Gap),
                                       Milliseconds = grp.Sum(x => x.Milliseconds),
                                       LastLap = grp.Max(x => x.Lap),
                                   }
                                   into grouping
                                   join winner in winnerGap
                                   on grouping.RaceID equals winner.RaceID
                                   select new
                                   {
                                       DriverName = grouping.DriverName,
                                       Result = grouping.DriverID == winner.DriverID ? grouping.Milliseconds : winner.BestTime - (grouping.Gap - winner.BestGap) - ((winner.BestTime / winner.LastLap) * (winner.LastLap - grouping.LastLap)),
                                       GapToWinner = grouping.Gap - winner.BestGap,
                                   }
                                   into final
                                   orderby final.Result descending
                                   select new GrandPrix()
                                   {
                                       DriverName = final.DriverName,
                                       Time = final.Result,
                                       GapToWinner = final.GapToWinner,
                                   };
            }
            else
            {
                var bestLaps = from lapTime2 in this.lapTimeRepo.GetAllToList()
                               join race in this.raceRepo.GetAllToList()
                               on lapTime2.RaceID equals race.RaceID
                               where race.Year == year && race.Round == round && laps.Contains(lapTime2.Lap)
                               select new
                               {
                                   RaceID = lapTime2.RaceID,
                                   Lap = lapTime2.Lap,
                                   Milliseconds = lapTime2.Milliseconds,
                               }
                           into lapTime
                               group lapTime by new { lapTime.RaceID, lapTime.Lap } into grp
                               select new
                               {
                                   RaceIDLap = grp.Key.RaceID + "," + grp.Key.Lap,
                                   BestLap = grp.Min(x => x.Milliseconds),
                               };

                var lapTimeWithGap = from lapTime in this.lapTimeRepo.GetAllToList()
                                     join race in this.raceRepo.GetAllToList()
                                     on lapTime.RaceID equals race.RaceID
                                     join bestLap in bestLaps
                                     on lapTime.RaceID + "," + lapTime.Lap equals bestLap.RaceIDLap
                                     where race.Year == year && race.Round == round && laps.Contains(lapTime.Lap)
                                     select new
                                     {
                                         RaceID = lapTime.RaceID,
                                         DriverID = lapTime.DriverID,
                                         Lap = lapTime.Lap,
                                         Gap = lapTime.Milliseconds - bestLap.BestLap,
                                         Milliseconds = lapTime.Milliseconds,
                                     };

                var lastLap = from lapTime2 in this.lapTimeRepo.GetAllToList()
                              join race in this.raceRepo.GetAllToList()
                              on lapTime2.RaceID equals race.RaceID
                              where race.Year == year && race.Round == round && laps.Contains(lapTime2.Lap)
                              select new
                              {
                                  RaceID = lapTime2.RaceID,
                                  Lap = lapTime2.Lap,
                              }
                              into lapTime
                              group lapTime by lapTime.RaceID into grp
                              select new
                              {
                                  RaceID = grp.Key,
                                  LastLap = grp.Max(x => x.Lap),
                              };

                var bestTime = from lapTime2 in this.lapTimeRepo.GetAllToList()
                               join race in this.raceRepo.GetAllToList()
                               on lapTime2.RaceID equals race.RaceID
                               where race.Year == year && race.Round == round && laps.Contains(lapTime2.Lap)
                               select new
                               {
                                   RaceID = lapTime2.RaceID,
                                   DriverID = lapTime2.DriverID,
                                   Milliseconds = lapTime2.Milliseconds,
                                   Lap = lapTime2.Lap,
                               }
                               into lapTime
                               group lapTime by new { lapTime.RaceID, lapTime.DriverID } into grp
                               select new
                               {
                                   RaceID = grp.Key.RaceID,
                                   DriverID = grp.Key.DriverID,
                                   Time = grp.Sum(x => x.Milliseconds),
                                   Laps = grp.Max(x => x.Lap),
                               }
                               into grouping
                               join lastLaps in lastLap
                               on grouping.RaceID equals lastLaps.RaceID
                               where grouping.Laps == lastLaps.LastLap
                               select new
                               {
                                   RaceID = grouping.RaceID,
                                   DriverID = grouping.DriverID,
                                   Time = grouping.Time,
                               }
                               into selection
                               group selection by selection.RaceID into grp
                               select new
                               {
                                   RaceID = grp.Key,
                                   BestTime = grp.Min(x => x.Time),
                               };

                var winnerAndBestTime = from lapTime2 in this.lapTimeRepo.GetAllToList()
                                        join race in this.raceRepo.GetAllToList()
                                        on lapTime2.RaceID equals race.RaceID
                                        where race.Year == year && race.Round == round && laps.Contains(lapTime2.Lap)
                                        select new
                                        {
                                            RaceID = lapTime2.RaceID,
                                            DriverID = lapTime2.DriverID,
                                            Milliseconds = lapTime2.Milliseconds,
                                        }
                                        into lapTime
                                        group lapTime by new { lapTime.RaceID, lapTime.DriverID } into grp
                                        select new
                                        {
                                            RaceID = grp.Key.RaceID,
                                            DriverID = grp.Key.DriverID,
                                            Time = grp.Sum(x => x.Milliseconds),
                                        }
                                        into selection
                                        join bestTimes in bestTime
                                        on selection.RaceID equals bestTimes.RaceID
                                        where selection.Time == bestTimes.BestTime
                                        select new
                                        {
                                            RaceID = selection.RaceID,
                                            DriverID = selection.DriverID,
                                            BestTime = bestTimes.BestTime,
                                        };

                var winnerGap = from lapTime in lapTimeWithGap
                                join driver in this.driverRepo.GetAllToList()
                                on lapTime.DriverID equals driver.DriverID
                                join race in this.raceRepo.GetAllToList()
                                on lapTime.RaceID equals race.RaceID
                                where race.Year == year && race.Round == round && laps.Contains(lapTime.Lap)
                                select new
                                {
                                    RaceID = lapTime.RaceID,
                                    DriverID = lapTime.DriverID,
                                    Gap = lapTime.Gap,
                                }
                                into data
                                group data by new { data.DriverID, data.RaceID } into grp
                                select new
                                {
                                    DriverID = grp.Key.DriverID,
                                    RaceID = grp.Key.RaceID,
                                    Gap = grp.Sum(x => x.Gap),
                                }
                                into final
                                join winner in winnerAndBestTime
                                on final.RaceID equals winner.RaceID
                                where final.DriverID == winner.DriverID
                                select new
                                {
                                    RaceID = winner.RaceID,
                                    DriverID = winner.DriverID,
                                    BestTime = winner.BestTime,
                                    BestGap = final.Gap,
                                }
                                into reallyFinal
                                join lap in lastLap
                                on reallyFinal.RaceID equals lap.RaceID
                                select new
                                {
                                    RaceID = reallyFinal.RaceID,
                                    DriverID = reallyFinal.DriverID,
                                    BestTime = reallyFinal.BestTime,
                                    BestGap = reallyFinal.BestGap,
                                    LastLap = lap.LastLap,
                                };

                this.grandprixes = from lapTime in lapTimeWithGap
                                   join driver in this.driverRepo.GetAllToList()
                                   on lapTime.DriverID equals driver.DriverID
                                   join race in this.raceRepo.GetAllToList()
                                   on lapTime.RaceID equals race.RaceID
                                   where race.Year == year && race.Round == round && laps.Contains(lapTime.Lap)
                                   select new
                                   {
                                       RaceID = lapTime.RaceID,
                                       DriverID = lapTime.DriverID,
                                       DriverName = driver.Name,
                                       Gap = lapTime.Gap,
                                       Lap = lapTime.Lap,
                                       Milliseconds = lapTime.Milliseconds,
                                   }
                                   into data
                                   group data by new { data.RaceID, data.DriverID, data.DriverName } into grp
                                   select new
                                   {
                                       RaceID = grp.Key.RaceID,
                                       DriverID = grp.Key.DriverID,
                                       DriverName = grp.Key.DriverName,
                                       Gap = grp.Sum(x => x.Gap),
                                       Milliseconds = grp.Sum(x => x.Milliseconds),
                                       LastLap = grp.Max(x => x.Lap),
                                   }
                                   into grouping
                                   join winner in winnerGap
                                   on grouping.RaceID equals winner.RaceID
                                   select new
                                   {
                                       DriverName = grouping.DriverName,
                                       Result = grouping.DriverID == winner.DriverID ? grouping.Milliseconds : winner.BestTime - (grouping.Gap - winner.BestGap) - ((winner.BestTime / winner.LastLap) * (winner.LastLap - grouping.LastLap)),
                                       GapToWinner = grouping.Gap - winner.BestGap,
                                   }
                                   into final
                                   orderby final.Result descending
                                   select new GrandPrix()
                                   {
                                       DriverName = final.DriverName,
                                       Time = final.Result,
                                       GapToWinner = final.GapToWinner,
                                   };
            }
        }

        /// <summary>
        /// Calculates the ratings of the drivers.
        /// </summary>
        private void CalculateDriverRatingsLogic()
        {
            this.driverRatingRepo.DeleteTable(this.driverRatings);

            var resultsByTeams = from result in this.resultRepo.GetAllToList()
                                 group result by new { result.ConstructorID, result.RaceID } into grp
                                 select new
                                 {
                                     RaceIDConstructorID = grp.Key.RaceID + "," + grp.Key.ConstructorID,
                                     SumOfPos = grp.Sum(x => x.Position),
                                     SumOfTime = grp.Sum(x => x.Milliseconds),
                                     Count = grp.Count(),
                                 };

            var nOParticipants = from result in this.resultRepo.GetAllToList()
                                 group result by result.RaceID into grp
                                 select new
                                 {
                                     RaceID = grp.Key,
                                     NOPart = grp.Count(),
                                 };

            var allowedStatus = new[] { 1, 2, 3, 4, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 45, 50, 53, 54, 55, 58, 62, 77, 81, 88, 90, 97, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 122, 123, 124, 125, 127, 128, 133, 134 };

            var groupResultDriverSorted = from result in this.resultRepo.GetAllToList()
                                          join driver in this.driverRepo.GetAllToList()
                                          on result.DriverID equals driver.DriverID
                                          where allowedStatus.Contains(result.StatusID)
                                          select new
                                          {
                                              DriverID = result.DriverID,
                                              Name = driver.Name,
                                              Position = result.Position,
                                          }
                                            into driverResult
                                          group driverResult by new { driverResult.DriverID, driverResult.Name } into grp
                                          select new
                                          {
                                              DriverID = grp.Key.DriverID,
                                              Name = grp.Key.Name,
                                              AvgFinPos = grp.Average(x => x.Position),
                                          };

            var groupResultDriver = from result in this.resultRepo.GetAllToList()
                                    join driver in this.driverRepo.GetAllToList()
                                    on result.DriverID equals driver.DriverID
                                    select new
                                    {
                                        DriverID = result.DriverID,
                                        Name = driver.Name,
                                        Position = result.Position,
                                    }
                                               into driverResult
                                    group driverResult by new { driverResult.DriverID, driverResult.Name } into grp
                                    select new
                                    {
                                        DriverID = grp.Key.DriverID,
                                        Name = grp.Key.Name,
                                        NORaces = grp.Count(),
                                    }
                                    into notSorted
                                    join sorted in groupResultDriverSorted
                                    on notSorted.DriverID equals sorted.DriverID
                                    select new
                                    {
                                        DriverID = sorted.DriverID,
                                        Name = sorted.Name,
                                        AvgFinPos = sorted.AvgFinPos,
                                        NORaces = notSorted.NORaces,
                                    };

            var driverRatingsToCalculate = from result in this.resultRepo.GetAllToList()
                                           join driverResult in groupResultDriver
                                           on result.DriverID equals driverResult.DriverID
                                           where allowedStatus.Contains(result.StatusID)
                                           select new
                                           {
                                               DriverID = result.DriverID,
                                               Name = driverResult.Name,
                                               AvgFinPos = driverResult.AvgFinPos,
                                               NORaces = driverResult.NORaces,
                                               RaceID = result.RaceID,
                                               ConstructorID = result.ConstructorID,
                                               Position = result.Position,
                                               Milliseconds = result.Milliseconds,
                                           }
                                                    into driverResult2
                                           join nOPart in nOParticipants
                                           on driverResult2.RaceID equals nOPart.RaceID
                                           select new
                                           {
                                               DriverID = driverResult2.DriverID,
                                               Name = driverResult2.Name,
                                               AvgFinPos = driverResult2.AvgFinPos,
                                               NORaces = driverResult2.NORaces,
                                               RaceIDConstructorID = driverResult2.RaceID + "," + driverResult2.ConstructorID,
                                               Position = driverResult2.Position,
                                               Milliseconds = driverResult2.Milliseconds,
                                               NOPart = nOPart.NOPart,
                                           }
                                                    into driverResult3
                                           join teamResult in resultsByTeams
                                           on driverResult3.RaceIDConstructorID equals teamResult.RaceIDConstructorID
                                           select new
                                           {
                                               DriverID = driverResult3.DriverID,
                                               Name = driverResult3.Name,
                                               AvgFinPos = driverResult3.AvgFinPos,
                                               NORaces = driverResult3.NORaces,
                                               AvgFinPosVsTM = teamResult.Count < 2 ? 0 : driverResult3.Position - ((teamResult.SumOfPos - driverResult3.Position) / (teamResult.Count - 1)),
                                               AvgFinTimeVsTM = teamResult.Count < 2 || driverResult3.Milliseconds == 0 || (teamResult.SumOfTime - driverResult3.Milliseconds) == 0 ? 0 : driverResult3.Milliseconds - ((teamResult.SumOfTime - driverResult3.Milliseconds) / (teamResult.Count - 1)),
                                               FinPosVSNOPart = (double)driverResult3.Position / (double)driverResult3.NOPart,
                                           }
                                                    into driverResult4
                                           group driverResult4 by new { driverResult4.DriverID, driverResult4.Name } into grp
                                           select new
                                           {
                                               DriverID = grp.Key.DriverID,
                                               Name = grp.Key.Name,
                                               AvgFinPos = grp.Average(x => x.AvgFinPos),
                                               NORaces = grp.Max(x => x.NORaces),
                                               AvgFinPosVsTM = grp.Average(x => x.AvgFinPosVsTM),
                                               AvgFinTimeVsTM = grp.Average(x => x.AvgFinTimeVsTM),
                                               FinPosVSNOPart = grp.Average(x => x.FinPosVSNOPart),
                                           }
                                           into data
                                           select new
                                           {
                                               DriverID = data.DriverID,
                                               Name = data.Name,
                                               AvgFinPos = data.AvgFinPos,
                                               NORaces = data.NORaces,
                                               AvgFinPosVsTM = data.AvgFinPosVsTM,
                                               AvgFinTimeVsTM = data.AvgFinTimeVsTM,
                                               FinPosVSNOPart = data.FinPosVSNOPart,
                                               DriverRating =
                                                  data.AvgFinPosVsTM > 2 || data.AvgFinPosVsTM < -2 ? // 1. IF: If the Average finishing position versus teammate is really good or really bad.
                                                    (data.AvgFinPosVsTM > 0 ? // 1.1 IF: this will be true, if AvgFinPosVsTM > 2
                                                        (data.AvgFinTimeVsTM > 2000 || data.AvgFinTimeVsTM < -2000 ? // 1.1.1 IF: If the Average finishing time versus teammate is really good or really bad (and the AvgFinPosVsTM > 2 from the previous if)
                                                            (data.AvgFinTimeVsTM > 0 ? // 1.1.1.1 IF: this will be true, if AvgFinTimeVsTM > 2000
                                                                (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - 5 - 2 : // AvgFinPosVsTM > 2 AND AvgFinTimeVsTM > 2000 (maximazing the loss from position and from time)
                                                            (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - 5 + 2) : // 1.1.1.1 ELSE: AvgFinPosVsTM > 2 AND AvgFinTimeVsTM < -2000 (maximazing the loss from position, and the win from time)
                                                        (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - 5 - (data.AvgFinTimeVsTM / (double)1000.0)) : // 1.1.1 ELSE: AvgFinPos > 2 (maximizing the loss from position)
                                                    (data.AvgFinTimeVsTM > 2000 || data.AvgFinTimeVsTM < -2000 ? // 1.1 ELSE: (1.2 IF) If the Average finishing time versus teammate is really good or really bad (and the AvgFinPosVsTM < 2)
                                                            (data.AvgFinTimeVsTM > 0 ? // 1.2.1 IF: this will be true, if AvgFinTimeVsTM > 2000
                                                                (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) + 5 - 2 : // AvgFinPosVsTM < -2 AND AvgFinTimeVsTM > 2000 (maximizing the win from position, and the loss from time)
                                                            (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) + 5 + 2) : // 1.2.1 ELSE: AvgFinPosVsTM < -2 AND AvgFinTimeVsTM < -2000 (maximizing the win from position and from time)
                                                        (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) + 5 - (data.AvgFinTimeVsTM / (double)1000.0))) : // 1.2 ELSE: AvgFinPos < -2 (maximizing the win from position)
                                                  (data.AvgFinTimeVsTM > 2000 || data.AvgFinTimeVsTM < 2000 ? // 1. ELSE: (2. IF) If the Average finishing time versus teammate is really good or really bad.
                                                    (data.AvgFinTimeVsTM > 0 ? // 2.1 IF: this will be true, if AvgFinTimeVsTM > 2000
                                                        (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - (2.5 * data.AvgFinPosVsTM) - 2 : // AvgFinTimeVsTM > 2000 (maximizing the loss from time)
                                                    (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - (2.5 * data.AvgFinPosVsTM) + 2) : // 2.1 ELSE: AvgFinTimeVsTM < -2000 (maximizing the win from time)
                                                  (((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - (2.5 * data.AvgFinPosVsTM) - (data.AvgFinTimeVsTM / (double)1000.0)), // 2 ELSE: we don't have to maximize any loss or win.
                                           }
                                           into driver
                                           select new
                                           {
                                               DriverID = driver.DriverID,
                                               Name = driver.Name,
                                               AvgFinPos = driver.AvgFinPos,
                                               NORaces = driver.NORaces,
                                               AvgFinPosVsTM = driver.AvgFinPosVsTM,
                                               AvgFinTimeVsTM = driver.AvgFinTimeVsTM,
                                               FinPosVSNOPart = driver.FinPosVSNOPart,
                                               DriverRating =
                                                         driver.DriverRating > 10 ?
                                                              10.0 :
                                                         (driver.DriverRating < 1 ?
                                                              1.0 :
                                                         driver.DriverRating),
                                           };

            var resultsWithDriverRatingsGrouping = from result in this.resultRepo.GetAllToList()
                                                   join driver in driverRatingsToCalculate
                                                   on result.DriverID equals driver.DriverID
                                                   select new
                                                   {
                                                       RaceID = result.RaceID,
                                                       ConstructorID = result.ConstructorID,
                                                       DriverRating = driver.DriverRating,
                                                   }
                                                   into result1
                                                   group result1 by new { result1.RaceID, result1.ConstructorID } into grp
                                                   select new
                                                   {
                                                       RaceIDConstructorID = grp.Key.RaceID + "," + grp.Key.ConstructorID,
                                                       SumOfRatings = grp.Sum(x => x.DriverRating),
                                                       Count = grp.Count(),
                                                   };

            IEnumerable<DriverRatings> finalExpandedDrivers = from result in this.resultRepo.GetAllToList()
                                                              join driver in driverRatingsToCalculate
                                                              on result.DriverID equals driver.DriverID
                                                              select new
                                                              {
                                                                  DriverID = driver.DriverID,
                                                                  Name = driver.Name,
                                                                  AvgFinPos = driver.AvgFinPos,
                                                                  NORaces = driver.NORaces,
                                                                  AvgFinPosVsTM = driver.AvgFinPosVsTM,
                                                                  AvgFinTimeVsTM = driver.AvgFinTimeVsTM,
                                                                  FinPosVSNOPart = driver.FinPosVSNOPart,
                                                                  DriverRating = driver.DriverRating,
                                                                  RaceIDConstructorID = result.RaceID + "," + result.ConstructorID,
                                                              }
                                                              into driver2
                                                              join result2 in resultsWithDriverRatingsGrouping
                                                                      on driver2.RaceIDConstructorID equals result2.RaceIDConstructorID
                                                              where result2.Count > 1
                                                              select new
                                                              {
                                                                  DriverID = driver2.DriverID,
                                                                  Name = driver2.Name,
                                                                  AvgFinPos = driver2.AvgFinPos,
                                                                  NORaces = driver2.NORaces,
                                                                  AvgFinPosVsTM = driver2.AvgFinPosVsTM,
                                                                  AvgFinTimeVsTM = driver2.AvgFinTimeVsTM,
                                                                  FinPosVSNOPart = driver2.FinPosVSNOPart,
                                                                  TMRating = (result2.SumOfRatings - driver2.DriverRating) / (double)(result2.Count - 1),
                                                              }
                                                              into driver3
                                                              group driver3 by new { driver3.DriverID, driver3.Name } into grp
                                                              select new
                                                              {
                                                                  DriverID = grp.Key.DriverID,
                                                                  Name = grp.Key.Name,
                                                                  AvgFinPos = grp.Max(x => x.AvgFinPos),
                                                                  NORaces = grp.Max(x => x.NORaces),
                                                                  AvgFinPosVsTM = grp.Max(x => x.AvgFinPosVsTM),
                                                                  AvgFinTimeVsTM = grp.Max(x => x.AvgFinTimeVsTM),
                                                                  FinPosVSNOPart = grp.Max(x => x.FinPosVSNOPart),
                                                                  TMRating = grp.Average(x => x.TMRating),
                                                              }
                                                              into data
                                                              select new DriverRatings()
                                                              {
                                                                  DriverID = data.DriverID,
                                                                  Name = data.Name,
                                                                  AvgFinPos = data.AvgFinPos,
                                                                  NORaces = data.NORaces,
                                                                  AvgFinPosVsTM = data.AvgFinPosVsTM,
                                                                  AvgFinTimeVsTM = data.AvgFinTimeVsTM,
                                                                  FinPosVSNOPart = data.FinPosVSNOPart,
                                                                  TMRating = data.TMRating,
                                                                  DriverRating =
                                                                                 data.AvgFinPosVsTM > 2 || data.AvgFinPosVsTM < -2 ? // 1. IF: If the Average finishing position versus teammate is really good or really bad.
                                                                                      (data.AvgFinPosVsTM > 0 ? // 1.1 IF: this will be true, if AvgFinPosVsTM > 2
                                                                                          (data.AvgFinTimeVsTM > 2000 || data.AvgFinTimeVsTM < -2000 ? // 1.1.1 IF: If the Average finishing time versus teammate is really good or really bad (and the AvgFinPosVsTM > 2 from the previous if)
                                                                                              (data.AvgFinTimeVsTM > 0 ? // 1.1.1.1 IF: this will be true, if AvgFinTimeVsTM > 2000
                                                                                                  ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - 5 - 2) * (data.TMRating / (double)15.0) : // AvgFinPosVsTM > 2 AND AvgFinTimeVsTM > 2000 (maximazing the loss from position and from time)
                                                                                              ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - 5 + 2) * (data.TMRating / (double)15.0)) : // 1.1.1.1 ELSE: AvgFinPosVsTM > 2 AND AvgFinTimeVsTM < -2000 (maximazing the loss from position, and the win from time)
                                                                                          ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - 5 - (data.AvgFinTimeVsTM / (double)1000.0)) * (data.TMRating / (double)15.0)) : // 1.1.1 ELSE: AvgFinPos > 2 (maximizing the loss from position)
                                                                                      (data.AvgFinTimeVsTM > 2000 || data.AvgFinTimeVsTM < -2000 ? // 1.1 ELSE: (1.2 IF) If the Average finishing time versus teammate is really good or really bad (and the AvgFinPosVsTM < 2)
                                                                                              (data.AvgFinTimeVsTM > 0 ? // 1.2.1 IF: this will be true, if AvgFinTimeVsTM > 2000
                                                                                                  ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) + 5 - 2) * (data.TMRating / (double)15.0) : // AvgFinPosVsTM < -2 AND AvgFinTimeVsTM > 2000 (maximizing the win from position, and the loss from time)
                                                                                              ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) + 5 + 2) * (data.TMRating / (double)15.0)) : // 1.2.1 ELSE: AvgFinPosVsTM < -2 AND AvgFinTimeVsTM < -2000 (maximizing the win from position and from time)
                                                                                          ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) + 5 - (data.AvgFinTimeVsTM / (double)1000.0)) * (data.TMRating / (double)15.0))) : // 1.2 ELSE: AvgFinPos < -2 (maximizing the win from position)
                                                                                  (data.AvgFinTimeVsTM > 2000 || data.AvgFinTimeVsTM < 2000 ? // 1. ELSE: (2. IF) If the Average finishing time versus teammate is really good or really bad.
                                                                                      (data.AvgFinTimeVsTM > 0 ? // 2.1 IF: this will be true, if AvgFinTimeVsTM > 2000
                                                                                          ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - (2.5 * data.AvgFinPosVsTM) - 2) * (data.TMRating / (double)15.0) : // AvgFinTimeVsTM > 2000 (maximizing the loss from time)
                                                                                      ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - (2.5 * data.AvgFinPosVsTM) + 2)) * (data.TMRating / (double)15.0) : // 2.1 ELSE: AvgFinTimeVsTM < -2000 (maximizing the win from time)
                                                                                  ((((4 * (1 - data.FinPosVSNOPart)) * ((double)data.NORaces / (double)4.5)) / (double)data.AvgFinPos) - (2.5 * data.AvgFinPosVsTM) - (data.AvgFinTimeVsTM / (double)1000.0)) * (data.TMRating / (double)15.0)), // 2 ELSE: we don't have to maximize any loss or win.
                                                              };

            foreach (DriverRatings item in finalExpandedDrivers)
            {
                this.driverRatingRepo.AddDriverRating(item);
            }

            this.newDataForDriverRatings = false;
        }

        /// <summary>
        /// Gets the data for the Championship view.
        /// </summary>
        /// <param name="year">Year to search for.</param>
        /// <param name="rounds">Rounds to search for.</param>
        private void GetChampionshipLogic(int year, int[] rounds)
        {
            if (rounds.Contains(0))
            {
                this.championship = from result in this.resultRepo.GetAllToList()
                                    join driver in this.driverRepo.GetAllToList()
                                    on result.DriverID equals driver.DriverID
                                    join race in this.raceRepo.GetAllToList()
                                    on result.RaceID equals race.RaceID
                                    where year == race.Year
                                    select new
                                    {
                                        DriverName = driver.Name,
                                        Points = result.Points,
                                    }
                                    into data
                                    group data by data.DriverName into grp
                                    select new
                                    {
                                        DriverName = grp.Key,
                                        Points = grp.Sum(x => x.Points),
                                    }
                                    into selection
                                    orderby selection.Points descending
                                    select new Championship()
                                    {
                                        DriverName = selection.DriverName,
                                        Points = selection.Points,
                                    };
            }
            else
            {
                this.championship = from result in this.resultRepo.GetAllToList()
                                    join driver in this.driverRepo.GetAllToList()
                                    on result.DriverID equals driver.DriverID
                                    join race in this.raceRepo.GetAllToList()
                                    on result.RaceID equals race.RaceID
                                    where year == race.Year && rounds.Contains(race.Round)
                                    select new
                                    {
                                        DriverName = driver.Name,
                                        Points = result.Points,
                                    }
                                   into data
                                    group data by data.DriverName into grp
                                    select new
                                    {
                                        DriverName = grp.Key,
                                        Points = grp.Sum(x => x.Points),
                                    }
                                     into selection
                                    orderby selection.Points descending
                                    select new Championship()
                                    {
                                        DriverName = selection.DriverName,
                                        Points = selection.Points,
                                    };
            }
        }
        #endregion
    }
}
