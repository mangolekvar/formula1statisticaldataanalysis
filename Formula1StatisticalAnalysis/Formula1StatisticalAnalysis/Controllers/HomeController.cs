﻿// <copyright file="HomeController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Runtime.InteropServices.WindowsRuntime;
    using System.Threading.Tasks;
    using Formula1StatisticalAnalysis.Data;
    using Formula1StatisticalAnalysis.Models;
    using Formula1StatisticalAnalysis.ViewModels;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Data.SqlClient;

    /// <summary>
    /// Controller class for Home page.
    /// </summary>
    public class HomeController : Controller
    {
        private ControllerLogic controllerLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="statusRepo">Status repository.</param>
        /// <param name="circuitRepo">Circuit repository.</param>
        /// <param name="raceRepo">Race repository.</param>
        /// <param name="driverRepo">Driver repository.</param>
        /// <param name="resultRepo">Result repository.</param>
        /// <param name="lapTimeRepo">Lap time repository.</param>
        /// <param name="driverRatingRepo">Driver rating repository.</param>
        /// <param name="failureRepo">Failure repository.</param>
        public HomeController(IStatusRepository statusRepo, ICircuitRepository circuitRepo, IRaceRepository raceRepo, IDriverRepository driverRepo, IResultRepository resultRepo, ILapTimeRepository lapTimeRepo, IDriverRatingRepository driverRatingRepo, IFailureRepository failureRepo)
        {
            this.controllerLogic = new ControllerLogic(statusRepo, circuitRepo, raceRepo, driverRepo, resultRepo, lapTimeRepo, driverRatingRepo, failureRepo);
        }

        /// <summary>
        /// Goes to Index view.
        /// </summary>
        /// <returns>Index view.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Goes to DriverRatings view.
        /// </summary>
        /// <returns>DriverRatings view.</returns>
        public IActionResult DriverRatings()
        {
            this.controllerLogic.CalculateDriverRatings();
            return this.View(this.controllerLogic.DriverRatings);
        }

        /// <summary>
        /// Goes to championship view if it was called via a Get method.
        /// </summary>
        /// <returns>Championship view.</returns>
        [HttpGet]
        public IActionResult Championship()
        {
            this.controllerLogic.GetChampionship();
            return this.View(this.controllerLogic.ChampionshipViewModel);
        }

        /// <summary>
        /// Goes to championship view if it was called via a Post method.
        /// </summary>
        /// <param name="year">Years to select.</param>
        /// <param name="round1">First Round selection.</param>
        /// <param name="round2">Seconds Round selection.</param>
        /// <returns>Championship view.</returns>
        [HttpPost]
        public IActionResult Championship(int year, int round1, int round2)
        {
            if (round1 < round2)
            {
                this.controllerLogic.GetChampionship(year, round1, round2);
            }
            else
            {
                this.controllerLogic.GetChampionship(year, round2, round1);
            }

            return this.View(this.controllerLogic.ChampionshipViewModel);
        }

        /// <summary>
        /// Goes to GrandPrix view if it was called via Get method.
        /// </summary>
        /// <returns>GrandPrix view.</returns>
        [HttpGet]
        public IActionResult GrandPrix()
        {
            this.controllerLogic.GetGrandPrixes();
            return this.View(this.controllerLogic.GrandPrixViewModel);
        }

        /// <summary>
        /// Goes to GrandPrix view if it was called via Post method.
        /// </summary>
        /// <param name="year">Year to select.</param>
        /// <param name="round">Round to select.</param>
        /// <param name="laps1">First laps selection.</param>
        /// <param name="laps2">Second laps selection.</param>
        /// <returns>GrandPrix view.</returns>
        [HttpPost]
        public IActionResult GrandPrix(int year, int round, int laps1, int laps2)
        {
            if (laps1 < laps2)
            {
                this.controllerLogic.GetGrandPrixes(year, round, laps1, laps2);
            }
            else
            {
                this.controllerLogic.GetGrandPrixes(year, round, laps2, laps1);
            }

            return this.View(this.controllerLogic.GrandPrixViewModel);
        }

        /// <summary>
        /// Goes to Failures view.
        /// </summary>
        /// <returns>Failures view.</returns>
        [HttpGet]
        public IActionResult Failures()
        {
            this.controllerLogic.CalculateFailures();
            return this.View(this.controllerLogic.FailureViewModel);
        }

        /// <summary>
        /// Goes to Failures view.
        /// </summary>
        /// <param name="year">Year to select.</param>
        /// <returns>Failures view.</returns>
        [HttpPost]
        public IActionResult Failures(int year)
        {
            this.controllerLogic.CalculateFailures(year);
            return this.View(this.controllerLogic.FailureViewModel);
        }

        /// <summary>
        /// Goes to LapTimePrediction view if it was called via Post method.
        /// </summary>
        /// <returns>LapTimePrediction view.</returns>
        [HttpGet]
        public IActionResult LapTimePrediction()
        {
            this.controllerLogic.GetLapTimePrediction();
            return this.View(this.controllerLogic.LapTimePredictionViewModel);
        }

        /// <summary>
        /// Goes to LapTimePrediction view if it was called via Post method.
        /// </summary>
        /// <param name="circuitID">CircuitID to select.</param>
        /// <returns>LapTimePrediction view.</returns>
        [HttpPost]
        public IActionResult LapTimePrediction(int circuitID)
        {
            this.controllerLogic.GetLapTimePrediction(circuitID);
            return this.View(this.controllerLogic.LapTimePredictionViewModel);
        }
    }
}
