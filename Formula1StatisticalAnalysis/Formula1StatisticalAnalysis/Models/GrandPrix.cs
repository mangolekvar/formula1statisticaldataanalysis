﻿// <copyright file="GrandPrix.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for Grand Prixs.
    /// </summary>
    public class GrandPrix
    {
        /// <summary>
        /// Gets or sets name of the driver.
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// Gets or sets racetime of the driver.
        /// </summary>
        public double Time { get; set; }

        /// <summary>
        /// Gets or sets gap to winner.
        /// </summary>
        public double GapToWinner { get; set; }
    }
}
