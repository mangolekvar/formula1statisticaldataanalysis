﻿// <copyright file="Result.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Result class.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Result"/> class.
        /// </summary>
        /// <param name="resultID">ID of the result.</param>
        /// <param name="raceID">ID of the race.</param>
        /// <param name="driverID">ID of the driver.</param>
        /// <param name="constructorID">ID of the constructor.</param>
        /// <param name="position">Position in which the driver finished.</param>
        /// <param name="points">How many points the driver received with this result.</param>
        /// <param name="milliseconds">How much time in milliseconds it took the driver to finish the race.</param>
        /// <param name="statusID">ID of the status.</param>
        public Result(int resultID, int raceID, int driverID, int constructorID, int position, double points, int milliseconds, int statusID)
        {
            this.ResultID = resultID;
            this.RaceID = raceID;
            this.DriverID = driverID;
            this.ConstructorID = constructorID;
            this.Position = position;
            this.Points = points;
            this.Milliseconds = milliseconds;
            this.StatusID = statusID;
        }

        /// <summary>
        /// Gets or sets ID of the result.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ResultID { get; set; }

        /// <summary>
        /// Gets or sets ID of the race.
        /// </summary>
        public int RaceID { get; set; }

        /// <summary>
        /// Gets or sets ID of the driver.
        /// </summary>
        public int DriverID { get; set; }

        /// <summary>
        /// Gets or sets ID of the constructor.
        /// </summary>
        public int ConstructorID { get; set; }

        /// <summary>
        /// Gets or sets the position of the driver at the end of the race.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets the amount of points the driver got for the result.
        /// </summary>
        public double Points { get; set; }

        /// <summary>
        /// Gets or sets how much time in milliseconds it took the driver to finish the race.
        /// </summary>
        public int Milliseconds { get; set; }

        /// <summary>
        /// Gets or sets ID of the status.
        /// </summary>
        public int StatusID { get; set; }
    }
}
