﻿// <copyright file="LapTimePrediction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Model class for lap time prediction.
    /// </summary>
    public class LapTimePrediction
    {
        /// <summary>
        /// Gets or sets lap number.
        /// </summary>
        public int Lap { get; set; }

        /// <summary>
        /// Gets or sets time in milliseconds.
        /// </summary>
        public double Milliseconds { get; set; }

        /// <summary>
        /// Gets or sets time in converted string format (minute:second.millisecond).
        /// </summary>
        public string Time { get; set; }
    }
}
