﻿// <copyright file="LapTime.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Lap Time class.
    /// </summary>
    public class LapTime
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LapTime"/> class.
        /// </summary>
        /// <param name="raceID">ID of the race.</param>
        /// <param name="driverID">ID of the driver.</param>
        /// <param name="lap">Lap number of the race.</param>
        /// <param name="milliseconds">Lap time in milliseconds.</param>
        public LapTime(int raceID, int driverID, int lap, int milliseconds)
        {
            this.RaceID = raceID;
            this.DriverID = driverID;
            this.Lap = lap;
            this.Milliseconds = milliseconds;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LapTime"/> class.
        /// </summary>
        public LapTime()
        {
        }

        /// <summary>
        /// Gets or sets ID of the race.
        /// </summary>
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RaceID { get; set; }

        /// <summary>
        /// Gets or sets ID of the driver.
        /// </summary>
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DriverID { get; set; }

        /// <summary>
        /// Gets or sets lap number of the race.
        /// </summary>
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Lap { get; set; }

        /// <summary>
        /// Gets or sets lap time in milliseconds.
        /// </summary>
        public int Milliseconds { get; set; }
    }
}
