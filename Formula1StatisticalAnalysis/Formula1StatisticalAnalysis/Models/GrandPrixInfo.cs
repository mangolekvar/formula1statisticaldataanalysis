﻿// <copyright file="GrandPrixInfo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Grand Prix info class.
    /// </summary>
    public class GrandPrixInfo
    {
        /// <summary>
        /// Gets or sets Year in which the race was held.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets round number.
        /// </summary>
        public int Round { get; set; }

        /// <summary>
        /// Gets or sets name of the grand prix.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Number of laps.
        /// </summary>
        public int NOLaps { get; set; }
    }
}
