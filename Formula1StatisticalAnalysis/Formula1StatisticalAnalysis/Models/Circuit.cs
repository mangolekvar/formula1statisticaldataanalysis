﻿// <copyright file="Circuit.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Circuit class.
    /// </summary>
    public class Circuit
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Circuit"/> class.
        /// </summary>
        /// <param name="circuitID">ID of the circuit.</param>
        /// <param name="name">Name of the circuit.</param>
        /// <param name="location">Location of the circuit.</param>
        /// <param name="country">Country of the circuit.</param>
        public Circuit(int circuitID, string name, string location, string country)
        {
            this.CircuitID = circuitID;
            this.Name = name;
            this.Location = location;
            this.Country = country;
        }

        /// <summary>
        /// Gets or sets ID for the circuit.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CircuitID { get; set; }

        /// <summary>
        /// Gets or sets the circuit's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the circuit's location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the circuit's country.
        /// </summary>
        public string Country { get; set; }
    }
}
