﻿// <copyright file="Status.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Status class.
    /// </summary>
    public class Status
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Status"/> class.
        /// </summary>
        /// <param name="statusID">ID of the status.</param>
        /// <param name="realStatus">Status in text.</param>
        public Status(int statusID, string realStatus)
        {
            this.StatusID = statusID;
            this.RealStatus = realStatus;
        }

        /// <summary>
        /// Gets or sets ID of the Status.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StatusID { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string RealStatus { get; set; }
    }
}
