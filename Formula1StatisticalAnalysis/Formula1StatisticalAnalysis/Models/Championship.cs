﻿// <copyright file="Championship.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Championship class.
    /// </summary>
    public class Championship
    {
        /// <summary>
        /// Gets or sets name of the driver.
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// Gets or sets points of the driver.
        /// </summary>
        public double Points { get; set; }
    }
}
