﻿// <copyright file="Race.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Race class.
    /// </summary>
    public class Race
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Race"/> class.
        /// </summary>
        /// <param name="raceID">ID of the race.</param>
        /// <param name="year">Year in which the race was held.</param>
        /// <param name="round">Which round of the season is this race.</param>
        /// <param name="circuitID">ID of the circuit in which the race was held.</param>
        /// <param name="name">Name of the race.</param>
        public Race(int raceID, int year, int round, int circuitID, string name)
        {
            this.RaceID = raceID;
            this.Year = year;
            this.Round = round;
            this.CircuitID = circuitID;
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets ID of the race.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RaceID { get; set; }

        /// <summary>
        /// Gets or sets year in which the race was held.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets which round of the season is the race.
        /// </summary>
        public int Round { get; set; }

        /// <summary>
        /// Gets or sets ID of the circuit which the race was held.
        /// </summary>
        [ForeignKey("CircuitDB.Circuits")]
        public int CircuitID { get; set; }

        /// <summary>
        /// Gets or sets the name of the race.
        /// </summary>
        public string Name { get; set; }
    }
}
