﻿// <copyright file="Driver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Driver class.
    /// </summary>
    public class Driver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Driver"/> class.
        /// </summary>
        /// <param name="driverID">ID of the driver.</param>
        /// <param name="name">Name of the driver.</param>
        public Driver(int driverID, string name)
        {
            this.DriverID = driverID;
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets ID of the driver.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DriverID { get; set; }

        /// <summary>
        /// Gets or sets name of the driver.
        /// </summary>
        public string Name { get; set; }
    }
}
