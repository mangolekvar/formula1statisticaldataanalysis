﻿// <copyright file="DriverRatings.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Driver rating class.
    /// </summary>
    public class DriverRatings
    {
        /// <summary>
        /// Gets or sets ID of the driver.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DriverID { get; set; }

        /// <summary>
        /// Gets or sets name of the driver.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets average finish position of the driver.
        /// </summary>
        public double AvgFinPos { get; set; }

        /// <summary>
        /// Gets or sets number of races in which the driver has participated.
        /// </summary>
        public int NORaces { get; set; }

        /// <summary>
        /// Gets or sets driver's average finishing position compared to teammate.
        /// </summary>
        public double AvgFinPosVsTM { get; set; }

        /// <summary>
        /// Gets or sets driver's average race time compared to teammate.
        /// </summary>
        public double AvgFinTimeVsTM { get; set; }

        /// <summary>
        /// Gets or sets driver's average finish position compared to the number of participants.
        /// </summary>
        public double FinPosVSNOPart { get; set; }

        /// <summary>
        /// gets or sets teammate's average rating.
        /// </summary>
        public double TMRating { get; set; }

        /// <summary>
        /// Gets or sets rating of the driver.
        /// </summary>
        public double DriverRating { get; set; }
    }
}
