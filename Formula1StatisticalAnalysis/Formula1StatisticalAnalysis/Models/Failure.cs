﻿// <copyright file="Failure.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1StatisticalAnalysis.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Failure class.
    /// </summary>
    public class Failure
    {
        /// <summary>
        /// Gets or sets status of failure.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the quantity of this type of failure.
        /// </summary>
        public int Quantity { get; set; }
    }
}
